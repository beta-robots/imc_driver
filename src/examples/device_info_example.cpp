#include "imc_driver/multi_device.h"

int main(int argc, char *argv[])
{
	char * ifname = argv[1];
	int num_devices = std::stoi(argv[2]);
	double timeout = 10.0; // seconds

	imc_driver::MultiDevice devices;

	// init devices
	devices.init(ifname, num_devices, timeout);

	for (unsigned int i = 0; i < num_devices; i++ )
	{
		std::cout << "Device " << i << ":" << std::endl;

		std::cout << "\tName: " << devices.getDeviceName(i) << std::endl;
		std::cout << "\tIdentity vendor ID: " << "0x" << std::setfill('0') << std::setw(8) << devices.getIdentityVendorID(i) << std::dec << std::endl;
		std::cout << "\tIdentity product code: " << "0x" << std::setfill('0') << std::setw(8) << devices.getIdentityProductCode(i) << std::dec << std::endl;
		std::cout << "\tIdentity revision number: " << "0x" << std::setfill('0') << std::setw(8) << devices.getIdentityRevisionNumber(i) << std::dec << std::endl;
		std::cout << "\tIdentity serial number: " << "0x" << std::setfill('0') << std::setw(8) << devices.getIdentitySerialNumber(i) << std::dec << std::endl;
		std::cout << "\tMotor type: " << "0x" << std::setfill('0') << std::setw(4) << devices.getMotorType(i) << std::endl;
		std::cout << "\tManufacturer device name: " << devices.getManufacturerDeviceName(i) << std::endl;
		std::cout << "\tManufacturer hardware name: " << devices.getManufacturerHardwareName(i) << std::endl;
		std::cout << "\tSoftware version: " << devices.getSoftwareVersion(i) << std::endl;
		std::cout << "\tHTTP drive catalog address: " << devices.getHTTPDriveCatalogAddress(i) << std::endl;
		std::cout << "\tDrive name: " << devices.getDriveName(i) << std::endl;

		std::vector<imc_driver::OperationMode> modes = devices.getSupportedDriveModes(i);
		std::cout << "\tSupported drive modes: ";
		for (auto & mode : modes)
			std::cout << mode << " ";
		std::cout << std::endl;

		std::cout << "\tRated torque: " << devices.getRatedTorque(i) << " Nm" << std::endl;
		std::cout << "\tMax current: " << devices.getMaxCurrent(i) << " A" << std::endl;
		std::cout << "\tMax motor speed: " << devices.getMaxMotorSpeed(i) << " rad/s" << std::endl;
		std::cout << "\tMax acceleration: " << devices.getMaxAcceleration(i) << " rad/s^2" << std::endl;
		std::cout << "\tMax deceleration: " << devices.getMaxDeceleration(i) << " rad/s^2" << std::endl;

		std::cout << "\tMax profile velocity: " << devices.getMaxProfileVelocity(i) << " rad/s" << std::endl;
		std::cout << "\tProfile acceleration: " << devices.getProfileAcceleration(i) << " rad/s^2" << std::endl;
		std::cout << "\tProfile deceleration: " << devices.getProfileDeceleration(i) << " rad/s^2" << std::endl;

		std::cout << "\tPosition control proportional constant: " << devices.getPositionControlProportionalConstant(i) << std::endl;
		std::cout << "\tPosition control integral constant: " << devices.getPositionControlIntegralConstant(i) << std::endl;
		std::cout << "\tPosition control derivative constant: " << devices.getPositionControlDerivativeConstant(i) << std::endl;
		std::cout << "\tVelocity control proportional constant: " << devices.getVelocityControlProportionalConstant(i) << std::endl;
		std::cout << "\tVelocity control integral constant: " << devices.getVelocityControlIntegralConstant(i) << std::endl;
		std::cout << "\tVelocity control derivative constant: " << devices.getVelocityControlDerivativeConstant(i) << std::endl;

		std::cout << "\tQuick stop option: " << devices.getQuickStopOptionCode(i) << std::endl;
		std::cout << "\tPositioning relative option: " << devices.getPositioningRelativeOption(i) << std::endl;
		std::cout << "\tPositioning rotary axis option: " << devices.getPositioningRotaryAxisOption(i) << std::endl;

		std::cout << "\tError register: " << unsigned(devices.getErrorRegister(i)) << std::endl;
		std::cout << "\tHistory number of errors: " << unsigned(devices.getHistoryNumberOfErrors(i)) << std::endl;
		std::vector<uint32> errors = devices.getHistoryErrors(i);
		std::cout << "\tHistory errors: " << std::hex ;
		for (auto & error : errors)
			std::cout << "0x" << std::setfill('0') << std::setw(4) << error << " ";
		std::cout << std::dec << std::endl;
		std::cout << "\tLast error code: " << "0x" << std::setfill('0') << std::setw(4) << devices.getLastErrorCode(i) << std::dec << std::endl;
		std::cout << "\tLast error string: " << devices.getLastErrorAsString(i) << std::endl;

		std::cout << "\tState machine state: " << devices.getStateMachineState(i) << std::endl;
		std::cout << "\tActual torque: " << devices.getActualTorque(i) << " Nm" << std::endl;
		std::cout << "\tActual velocity: " << devices.getActualVelocity(i) << " rad/s" << std::endl;
		std::cout << "\tActual position: " << devices.getActualPosition(i) << " rad" << std::endl;
		std::cout << "\tActual following error: " << devices.getActualFollowingError(i) << " rad" << std::endl;
		std::cout << "\tDrive temperature: " << devices.getDriveTemperature(i) << " ºC" << std::endl;
		std::cout << "\tPosition demand value: " << devices.getPositionDemandValue(i) << " rad" << std::endl;
		std::cout << "\tPosition range limits: [" << devices.getMinPositionRangeLimit(i) << ", " << devices.getMaxPositionRangeLimit(i) << "] rad" << std::endl;

		std::cout << std::endl;
	}

	devices.close();

	return 0;
}
