#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <iostream>
#include <iomanip>
#include <thread>
#include <unistd.h>
#include <vector>
#include <chrono>
#include <cmath>

#include "soem/ethercattype.h"
#include "soem/nicdrv.h"
#include "soem/ethercatbase.h"
#include "soem/ethercatmain.h"
#include "soem/ethercatdc.h"
#include "soem/ethercatcoe.h"
#include "soem/ethercatfoe.h"
#include "soem/ethercatconfig.h"
#include "soem/ethercatprint.h"

template<class uint8_16_32>
void writeSDO(const uint16 slave, const uint16 idx, const uint8 sub, uint8_16_32 *buf, const uint8_16_32 value)
{
	uint8_16_32 data = value;
	int __s = sizeof(data);
	int __ret = ec_SDOwrite(slave, idx, sub, FALSE, __s, &data, EC_TIMEOUTRXM);
	*buf = data;
};

template<class uint8_16_32>
void readSDO(const uint16 slave, const uint16 idx, const uint8 sub, uint8_16_32 *buf)
{
	uint8_16_32 data = *buf;
	int __s = sizeof(data);
	int __ret = ec_SDOread(slave, idx, sub, FALSE, &__s, &data, EC_TIMEOUTRXM);
	*buf = data;
};

int main(int argc, char *argv[])
{
	double loop_duration = 5.0; // duration of each testing loop [s]
	char * ifname = argv[1];

	if ( !ec_init(ifname) )
	{
		std::cout << "No socket connection on "<< ifname << ". Excecute as root" << std::endl;
		return -1;
	}
	std::cout << "ec_init on "<< ifname << " succeeded" << std::endl;

	if ( ec_config_init(FALSE) > 0)
	{
		std::cout << "Slaves count: " << ec_slavecount << std::endl;
		for ( int i=0; i < ec_slavecount; i++ )
			std::cout << "Slave " << i << " name: " << ec_slave[i+1].name << std::endl;
	}
	else
	{
		std::cout << "Error configuring ethercat" << std::endl;
		return -1;
	}

	// configure network memory map
	char io_map[4096];
	uint32 data32;
	uint8 data8;
	uint16 data16;
	for ( uint16 i = 0; i < ec_slavecount; i++)
	{
		// define TPDOs (from nodes to master)
		writeSDO<uint32>(i+1, 0x1A00, 1, &data32, 0x60410010); // Status word, 0x6041 00 16bits
		writeSDO<uint32>(i+1, 0x1A00, 2, &data32, 0x606C0020); // Velocity, 0x606C 00 32bits
		writeSDO<uint32>(i+1, 0x1A00, 3, &data32, 0x60770010); // Torque, 0x6077 00 16bits
		writeSDO<uint32>(i+1, 0x1A00, 4, &data32, 0x60640020); // Position, 0x6064 00 32bits
		writeSDO<uint32>(i+1, 0x1A00, 5, &data32, 0x60610008); // Operation, 0x6061 00 8bits
		writeSDO<uint32>(i+1, 0x1A00, 6, &data32, 0x60F40008); // Following error, 0x60F4 00 8bits

		// define RPDOs (from master to nodes)
		/*
		RxPDOx, is left by default. Only RxPDO4 will be used:
		0x6040 0 16 Control word
		0x607A 0 32 Target position
		0x60FF 0 32 Target velocity
		0x6071 0 16 Target torque
		0x6072 0 16 Max torque
		0x6060 0 8 Mode of operation
		*/
		// TPDO1 is active by default, but RPDO4 is not. The following line will set the RPDO4 instead of the RPDO1
		writeSDO<uint32>(i+1, 0x1C12, 1, &data32, 0x1603);

		// command reference to network
		writeSDO<uint8>(i+1, 0x2430, 0, &data8, 0x00);
	}
	int used_memory = ec_config_map(&io_map);
	if ( used_memory <= sizeof(io_map) )
		std::cout << "Memory check ok, used: " << used_memory << std::endl;
	else
		std::cout << "Memory not ok, used: " << used_memory << ", expected: " << sizeof(io_map) << std::endl;

	ec_configdc();

	// wait for ethercat state to be SAFE_OP
	if ( ec_statecheck(0, EC_STATE_SAFE_OP,  EC_TIMEOUTSTATE * 4) > 0 )
		std::cout << "Safe OP reached" << std::endl;
	else
		std::cout << "Error reaching safe OP" << std::endl;

	// move ethercat state machines to OPERATIONAL
	ec_slave[0].state = EC_STATE_OPERATIONAL;
	ec_send_processdata(); // send one valid process data to make outputs in slaves happy
	ec_receive_processdata(EC_TIMEOUTRET);
	ec_writestate(0); // request OP state for all slaves
	int chk = 40; // 40 trials!
	do // wait for all slaves to reach OP state
	{
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		ec_statecheck(0, EC_STATE_OPERATIONAL, 50000);
	}
	while (chk-- && (ec_slave[0].state != EC_STATE_OPERATIONAL));
	if ( ec_slave[0].state != EC_STATE_OPERATIONAL )
	{
		std::cout << "Not all slaves reached operational state." << std::endl;
		return -1;
	}
	std::cout << "Operational state reached for all slaves." << std::endl;
	ec_readstate();
	for(int i = 0; i<ec_slavecount ; i++)
	{
		std::cout << "Slave " << i << ", state: " << ec_slave[i+1].state << std::endl;
	}

	// get data from slaves
	std::vector<double> encoder_increments_per_turn(ec_slavecount), motor_rated_torque(ec_slavecount), motor_max_current(ec_slavecount), motor_max_speed(ec_slavecount);
	uint32 encoder_increments, motor_revolutions, rated_torque, max_current, max_speed;
	for(int i = 0; i<ec_slavecount ; i++)
	{
		readSDO<uint32>(i+1, 0x608F, 0x01, &encoder_increments); // Encoder increments
		readSDO<uint32>(i+1, 0x608F, 0x02, &motor_revolutions); // Motor revolutions
		encoder_increments_per_turn.at(i) = (double)encoder_increments/(double)motor_revolutions;
		std::cout << "Slave " << i << ", encoder increments per turn: " << encoder_increments_per_turn.at(i) << std::endl;

		readSDO<uint32>(i+1, 0x6076, 0x00, &rated_torque);
		readSDO<uint32>(i+1, 0x6075, 0x00, &max_current);
		readSDO<uint32>(i+1, 0x6080, 0x00, &max_speed);
		motor_rated_torque.at(i) = (double)(rated_torque)*0.001;
		motor_max_current.at(i) = (double)(max_current)*0.001;
		motor_max_speed.at(i) = (double)(max_speed)*2.0*M_PI/encoder_increments_per_turn.at(i);
		std::cout << "Slave " << i << ", motor max torque: " << motor_rated_torque.at(i) << std::endl;
		std::cout << "Slave " << i << ", motor max current: " << motor_max_current.at(i) << std::endl;
		std::cout << "Slave " << i << ", motor max speed: " << motor_max_speed.at(i) << std::endl;
	}

	// check state machine states
	std::cout << std::endl << "State Machine States check:" << std::endl;
	ec_send_processdata();
	ec_receive_processdata(EC_TIMEOUTRET);
	//get status word, [bit coded]
	std::vector<uint16> status_word(ec_slavecount);
	for(int i = 0; i<ec_slavecount ; i++)
	{
		status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
		std::cout << "Slave " << i << ", sm state: 0x" << std::hex << std::setfill('0') << std::setw(4) << status_word.at(i) << std::endl;
	}

	// reset drives that are in fault
	std::cout << std::endl << "Reset faults:" << std::endl;
	uint16 control_word;
	control_word = 0x0080;	// fault reset
	for(int i = 0; i<ec_slavecount ; i++)
	{
		*(ec_slave[i+1].outputs) = control_word & 0x00FF;
		*(ec_slave[i+1].outputs+1) = control_word >> 8;
	}
	std::chrono::time_point<std::chrono::system_clock> start_time = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_time = std::chrono::system_clock::now() - start_time;
	bool all_ok = false;
	while ( (elapsed_time.count() <= loop_duration) && (!all_ok) )
	{
		all_ok = true;
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		for(int i = 0; i < ec_slavecount ; i++)
		{
			status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
			if ( (status_word.at(i) & 0x004F) != 0x40 )
				all_ok = false;
		}
		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	std::cout << "elapsed time: " << elapsed_time.count() << std::endl;
	if ( !all_ok )
		std::cout << "timed out" << std::endl;
	for(int i = 0; i < ec_slavecount ; i++)
	{
		status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
		std::cout << "Slave " << i << ", sm state: 0x" << std::hex << std::setfill('0') << std::setw(4) << status_word.at(i) << std::endl;
	}

	// transition to ready to switch on
	std::cout << std::endl << "Transition to switch on:" << std::endl;
	control_word = 0x0006;	// shutdown to go to state 0xXX21
	for(int i = 0; i<ec_slavecount ; i++)
	{
		*(ec_slave[i+1].outputs) = control_word & 0x00FF;
		*(ec_slave[i+1].outputs+1) = control_word >> 8;
	}
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	all_ok = false;
	while ( (elapsed_time.count() <= loop_duration) && (!all_ok) )
	{
		all_ok = true;
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		for(int i = 0; i < ec_slavecount ; i++)
		{
			status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
			if ( (status_word.at(i) & 0x006F) != 0x21 )
				all_ok = false;
		}
		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	std::cout << "elapsed time: " << elapsed_time.count() << std::endl;
	if ( !all_ok )
		std::cout << "timed out" << std::endl;
	for(int i = 0; i < ec_slavecount ; i++)
	{
		status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
		std::cout << "Slave " << i << " , sm state: 0x" << std::hex << std::setfill('0') << std::setw(4) << status_word.at(i) << std::endl;
	}

	// select operation mode to profile velocity
	std::cout << std::endl << "Select operation mode:" << std::endl;
	std::vector<uint8> operation_mode(ec_slavecount);
	for(int i = 0; i < ec_slavecount ; i++)
	{
		operation_mode.at(i) = *(ec_slave[i+1].inputs + 12);
		std::cout << "Slave " << i << ", operation mode before: " << (int)operation_mode.at(i) << std::endl;
	}
	uint8 requested_operation_mode = 3;
	for(int i = 0; i<ec_slavecount ; i++)
	{
		*(ec_slave[i+1].outputs+14) = requested_operation_mode;
	}
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	all_ok = false;
	while ( (elapsed_time.count() <= loop_duration) && (!all_ok) )
	{
		all_ok = true;
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		for(int i = 0; i < ec_slavecount ; i++)
		{
			operation_mode.at(i) = *(ec_slave[i+1].inputs + 12);
			if ( operation_mode.at(i) != requested_operation_mode )
				all_ok = false;
		}
		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	std::cout << "elapsed time: " << elapsed_time.count() << std::endl;
	if ( !all_ok )
		std::cout << "timed out" << std::endl;
	for(int i = 0; i < ec_slavecount ; i++)
	{
		operation_mode.at(i) = *(ec_slave[i+1].inputs + 12);
		std::cout << "Slave " << i << ", operation mode after: " << (int)operation_mode.at(i) << std::endl;
	}

	// Set Max Torque in operation
	std::cout << std::endl << "Set max torque:" << std::endl;
	int16 max_torque = 500; // per mil of rated torque -> we will work at half the rated torque that the motor can deliver
	for(int i = 0; i<ec_slavecount ; i++)
	{
		*(ec_slave[i+1].outputs+12) = max_torque & 0x00FF;
		*(ec_slave[i+1].outputs+13) = max_torque >> 8;
		std::cout << "Max torque in operation set of Slave " << i << ": " << std::dec << max_torque << " per mil of rated" << std::endl;
	}

	// transition to operation enable
	std::cout << std::endl << "Transition to operation enable:" << std::endl;
	control_word = 0x000F; // (switch on + enable operation) command to go to state 0xXX27
	for(int i = 0; i<ec_slavecount ; i++)
	{
		*(ec_slave[i+1].outputs) = control_word & 0x00FF;
		*(ec_slave[i+1].outputs+1) = control_word >> 8;
	}
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	all_ok = false;
	while ( (elapsed_time.count() <= loop_duration) && (!all_ok) )
	{
		all_ok = true;
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		for(int i = 0; i < ec_slavecount ; i++)
		{
			status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
			if ( (status_word.at(i) & 0x006F) != 0x27 )
				all_ok = false;
		}
		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	std::cout << "elapsed time: " << elapsed_time.count() << std::endl;
	if ( !all_ok )
		std::cout << "timed out" << std::endl;
	for(int i = 0; i < ec_slavecount ; i++)
	{
		status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
		std::cout << "Slave " << i << ", sm state: 0x" << std::hex << std::setfill('0') << std::setw(4) << status_word.at(i) << std::endl;
	}
	std::cout << std::dec;

	// set profile parameters
	std::cout << std::endl << "Set profile velocity params:" << std::endl;
	uint32 profile_acceleration, profile_deceleration, max_velocity, max_acceleration, max_deceleration;
	double MAXIMUM_ACCELERATION = 600.0;// rad/s^2
	double  MAXIMUM_VELOCITY = 314.0;	// rad/s
	for ( unsigned int i = 0; i < ec_slavecount; i++ )
	{
		max_acceleration = (uint32)( MAXIMUM_ACCELERATION * encoder_increments_per_turn.at(i) / (2.0 * M_PI) );
		max_velocity = (uint32)( MAXIMUM_VELOCITY * encoder_increments_per_turn.at(i) / (2.0 * M_PI) );
		writeSDO<uint32>(i+1, 0x6083, 0x00, &data32, max_acceleration);	// profile acceleration
		writeSDO<uint32>(i+1, 0x6084, 0x00, &data32, max_acceleration);	// profile deceleration
		writeSDO<uint32>(i+1, 0x607F, 0x00, &data32, max_velocity);		// max_velocity
		writeSDO<uint32>(i+1, 0x60C5, 0x00, &data32, max_acceleration);	// max acceleration
		writeSDO<uint32>(i+1, 0x60C6, 0x00, &data32, max_acceleration);	// max decleration
	}
	std::cout << "Parameters set" << std::endl;

	// send a velocity command
	std::cout << std::endl << "Send a velocity command:" << std::endl;
	std::cout << "Same velocity all devices -> running" << std::endl;
	double target_velocity = 2.0*M_PI; // rad/s
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	while( elapsed_time.count() <= 1.0 )
	{
		for(int i = 0; i < ec_slavecount; i++)
		{
			control_word = 0x000F; // keep enabling operation
			*(ec_slave[i+1].outputs) = control_word & 0x00FF;
			*(ec_slave[i+1].outputs+1) = control_word >> 8;

			// transform rad/s to encoder pulses as integers
			int32 target_velocity_encoder = (int32)( target_velocity * encoder_increments_per_turn.at(i) / ( 2.0 * M_PI) );
			*(ec_slave[i+1].outputs+6) = target_velocity_encoder & 0x000000FF;
			*(ec_slave[i+1].outputs+7) = (target_velocity_encoder >> 8) & 0x000000FF;
			*(ec_slave[i+1].outputs+8) = (target_velocity_encoder >> 16) & 0x000000FF;
			*(ec_slave[i+1].outputs+9) = (target_velocity_encoder >> 24) & 0x000000FF;
		}
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		// get feedback
		/*for(int i = 0; i < ec_slavecount; i++)
		{
			int32 actual_velocity = (*(ec_slave[i+1].inputs + 5) << 24) | (*(ec_slave[i+1].inputs + 4)<<16) | (*(ec_slave[i+1].inputs + 3) << 8) | (*(ec_slave[i+1].inputs + 2));
			std::cout << "\tVelocity " << i << ": " << ((double)actual_velocity) * 2.0 * M_PI / encoder_increments_per_turn.at(i) << std::endl;
		}*/

		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}

	std::cout << "Opposite direction velocity -> running" << std::endl;
	target_velocity = 3.0*M_PI;
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	while( elapsed_time.count() <= 1.0 )
	{
		for(int i = 0; i < ec_slavecount; i++)
		{
			control_word = 0x000F; // keep enabling operation
			*(ec_slave[i+1].outputs) = control_word & 0x00FF;
			*(ec_slave[i+1].outputs+1) = control_word >> 8;

			// transform rad/s to encoder pulses as integers
			int32 target_velocity_encoder = (int32)( target_velocity * encoder_increments_per_turn.at(i) / ( 2.0 * M_PI) );
			*(ec_slave[i+1].outputs+6) = target_velocity_encoder & 0x000000FF;
			*(ec_slave[i+1].outputs+7) = (target_velocity_encoder >> 8) & 0x000000FF;
			*(ec_slave[i+1].outputs+8) = (target_velocity_encoder >> 16) & 0x000000FF;
			*(ec_slave[i+1].outputs+9) = (target_velocity_encoder >> 24) & 0x000000FF;
		}
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		// get feedback
		/*for(int i = 0; i < ec_slavecount; i++)
		{
			int32 actual_velocity = (*(ec_slave[i+1].inputs + 5) << 24) | (*(ec_slave[i+1].inputs + 4)<<16) | (*(ec_slave[i+1].inputs + 3) << 8) | (*(ec_slave[i+1].inputs + 2));
			std::cout << "\tVelocity " << i << ": " << ((double)actual_velocity) * 2.0 * M_PI / encoder_increments_per_turn.at(i) << std::endl;
		}*/

		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}


	std::cout << "Devices back at zero velocity" << std::endl;
	target_velocity = 0.0;
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	while( elapsed_time.count() <= 1.0 )
	{
		for(int i = 0; i < ec_slavecount; i++)
		{
			control_word = 0x000F; // keep enabling operation
			*(ec_slave[i+1].outputs) = control_word & 0x00FF;
			*(ec_slave[i+1].outputs+1) = control_word >> 8;

			// transform rad/s to encoder pulses as integers
			int32 target_velocity_encoder = (int32)( target_velocity * encoder_increments_per_turn.at(i) / ( 2.0 * M_PI) );
			*(ec_slave[i+1].outputs+6) = target_velocity_encoder & 0x000000FF;
			*(ec_slave[i+1].outputs+7) = (target_velocity_encoder >> 8) & 0x000000FF;
			*(ec_slave[i+1].outputs+8) = (target_velocity_encoder >> 16) & 0x000000FF;
			*(ec_slave[i+1].outputs+9) = (target_velocity_encoder >> 24) & 0x000000FF;
		}
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);

		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}

	// Disable all devices
	std::cout << std::endl << "Disable voltage on all devices:" << std::endl;
	control_word = 0x0000;	// disable voltage
	for(int i = 0; i<ec_slavecount ; i++)
	{
		*(ec_slave[i+1].outputs) = control_word & 0x00FF;
		*(ec_slave[i+1].outputs+1) = control_word >> 8;
	}
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	all_ok = false;
	while ( (elapsed_time.count() <= loop_duration) && (!all_ok) )
	{
		all_ok = true;
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		for(int i = 0; i < ec_slavecount ; i++)
		{
			status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
			if ( (status_word.at(i) & 0x004F) != 0x40 )
				all_ok = false;
		}
		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	std::cout << "elapsed time: " << elapsed_time.count() << std::endl;
	if ( !all_ok )
		std::cout << "timed out" << std::endl;
	for(int i = 0; i < ec_slavecount ; i++)
	{
		status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
		std::cout << "Slave " << i << " , sm state: 0x" << std::hex << std::setfill('0') << std::setw(4) << status_word.at(i) << std::endl;
	}

	// transition to ready to switch on
	std::cout << std::endl << "Transition to switch on:" << std::endl;
	control_word = 0x0006;	// shutdown to go to state 0xXX21
	for(int i = 0; i<ec_slavecount ; i++)
	{
		*(ec_slave[i+1].outputs) = control_word & 0x00FF;
		*(ec_slave[i+1].outputs+1) = control_word >> 8;
	}
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	all_ok = false;
	while ( (elapsed_time.count() <= loop_duration) && (!all_ok) )
	{
		all_ok = true;
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		for(int i = 0; i < ec_slavecount ; i++)
		{
			status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
			if ( (status_word.at(i) & 0x006F) != 0x21 )
				all_ok = false;
		}
		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	std::cout << "elapsed time: " << elapsed_time.count() << std::endl;
	if ( !all_ok )
		std::cout << "timed out" << std::endl;
	for(int i = 0; i < ec_slavecount ; i++)
	{
		status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
		std::cout << "Slave " << i << " , sm state: 0x" << std::hex << std::setfill('0') << std::setw(4) << status_word.at(i) << std::endl;
	}

	// Change to profile torque mode
	std::cout << std::endl << "Change to profile torque mode:" << std::endl;
	for(int i = 0; i < ec_slavecount ; i++)
	{
		operation_mode.at(i) = *(ec_slave[i+1].inputs + 12);
		std::cout << "Slave " << i << ", operation mode before: " << (int)operation_mode.at(i) << std::endl;
	}
	requested_operation_mode = 4;
	for(int i = 0; i<ec_slavecount ; i++)
	{
		*(ec_slave[i+1].outputs+14) = requested_operation_mode;
	}
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	all_ok = false;
	while ( (elapsed_time.count() <= loop_duration) && (!all_ok) )
	{
		all_ok = true;
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		for(int i = 0; i < ec_slavecount ; i++)
		{
			operation_mode.at(i) = *(ec_slave[i+1].inputs + 12);
			if ( operation_mode.at(i) != requested_operation_mode )
				all_ok = false;
		}
		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	std::cout << "elapsed time: " << elapsed_time.count() << std::endl;
	if ( !all_ok )
		std::cout << "timed out" << std::endl;
	for(int i = 0; i < ec_slavecount ; i++)
	{
		operation_mode.at(i) = *(ec_slave[i+1].inputs + 12);
		std::cout << "Slave " << i << ", operation mode after: " << (int)operation_mode.at(i) << std::endl;
	}

	// transition to operation enable
	std::cout << std::endl << "Transition to operation enable:" << std::endl;
	control_word = 0x000F; // (switch on + enable operation) command to go to state 0xXX27
	for(int i = 0; i<ec_slavecount ; i++)
	{
		*(ec_slave[i+1].outputs) = control_word & 0x00FF;
		*(ec_slave[i+1].outputs+1) = control_word >> 8;
	}
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	all_ok = false;
	while ( (elapsed_time.count() <= loop_duration) && (!all_ok) )
	{
		all_ok = true;
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		for(int i = 0; i < ec_slavecount ; i++)
		{
			status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
			if ( (status_word.at(i) & 0x006F) != 0x27 )
				all_ok = false;
		}
		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	std::cout << "elapsed time: " << elapsed_time.count() << std::endl;
	if ( !all_ok )
		std::cout << "timed out" << std::endl;
	for(int i = 0; i < ec_slavecount ; i++)
	{
		status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
		std::cout << "Slave " << i << ", sm state: 0x" << std::hex << std::setfill('0') << std::setw(4) << status_word.at(i) << std::endl;
	}
	std::cout << std::dec;

	// send torque command
	std::cout << std::endl << "Send torque commmand:" << std::endl;
	std::cout << "\tTorque small -> running" << std::endl;
	double target_torque = 0.1; //Nm
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	while( elapsed_time.count() <= loop_duration )
	{
		for(int i = 0; i < ec_slavecount; i++)
		{
			control_word = 0x000F; // keep enabling operation
			*(ec_slave[i+1].outputs) = control_word & 0x00FF;
			*(ec_slave[i+1].outputs+1) = control_word >> 8;

			// transform Nm to encoder pulses as integers
			int32 target_torque_encoder = (int32)( target_torque * 1000.0 / motor_rated_torque.at(i) );
			*(ec_slave[i+1].outputs+10) = target_torque_encoder & 0x00FF;
			*(ec_slave[i+1].outputs+11) = (target_torque_encoder >> 8) & 0x00FF;
			//std::cout << "target velocity encoder: " << target_velocity_encoder << std::endl;
		}
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);

		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	std::cout << "\tTorque medium -> running" << std::endl;
	target_torque = -0.2; //Nm
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	while( elapsed_time.count() <= loop_duration )
	{
		for(int i = 0; i < ec_slavecount; i++)
		{
			control_word = 0x000F; // keep enabling operation
			*(ec_slave[i+1].outputs) = control_word & 0x00FF;
			*(ec_slave[i+1].outputs+1) = control_word >> 8;

			// transform rad/s to encoder pulses as integers
			int32 target_torque_encoder = (int32)( target_torque * 1000.0 / motor_rated_torque.at(i) );
			*(ec_slave[i+1].outputs+10) = target_torque_encoder & 0x00FF;
			*(ec_slave[i+1].outputs+11) = (target_torque_encoder >> 8) & 0x00FF;
		}
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);

		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	std::cout << "\tTorque zero -> running" << std::endl;
	target_torque = 0.0;
	for(int i = 0; i < ec_slavecount; i++)
	{
		control_word = 0x000F; // keep enabling operation
		*(ec_slave[i+1].outputs) = control_word & 0x00FF;
		*(ec_slave[i+1].outputs+1) = control_word >> 8;

		// transform rad/s to encoder pulses as integers
		int32 target_torque_encoder = (int32)( target_torque * 1000.0 / motor_rated_torque.at(i) );
		*(ec_slave[i+1].outputs+10) = target_torque_encoder & 0x00FF;
		*(ec_slave[i+1].outputs+11) = (target_torque_encoder >> 8) & 0x00FF;
	}
	ec_send_processdata();
	ec_receive_processdata(EC_TIMEOUTRET);

	// Disable all devices
	std::cout << std::endl << "Disable voltage on all devices:" << std::endl;
	control_word = 0x0000;	// disable voltage
	for(int i = 0; i<ec_slavecount ; i++)
	{
		*(ec_slave[i+1].outputs) = control_word & 0x00FF;
		*(ec_slave[i+1].outputs+1) = control_word >> 8;
	}
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	all_ok = false;
	while ( (elapsed_time.count() <= loop_duration) && (!all_ok) )
	{
		all_ok = true;
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		for(int i = 0; i < ec_slavecount ; i++)
		{
			status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
			if ( (status_word.at(i) & 0x004F) != 0x40 )
				all_ok = false;
		}
		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	std::cout << "elapsed time: " << elapsed_time.count() << std::endl;
	if ( !all_ok )
		std::cout << "timed out" << std::endl;
	for(int i = 0; i < ec_slavecount ; i++)
	{
		status_word.at(i) = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
		std::cout << "Slave " << i << ", sm state: 0x" << std::hex << std::setfill('0') << std::setw(4) << status_word.at(i) << std::endl;
	}

	// Close ethercat interface
	std::cout << std::endl << "Close ethercat interface" << std::endl;
	ec_slave[0].state = EC_STATE_INIT;
	ec_writestate(0); // request INIT state for all slaves
	ec_close(); // stop SOEM, close socket

	return 0;
}
