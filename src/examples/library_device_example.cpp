#include "imc_driver/multi_device.h"

int main(int argc, char *argv[])
{
	char * ifname = argv[1];
	int num_devices = std::stoi(argv[2]);
	double timeout = 10.0; // seconds
	double loop_duration = 5.0; // duration of each testing loop [s]
	unsigned int loop_period = 10; // loop period (inverse of r/w rate) [ms]

	std::cout << std::endl << "STARTING EXAMPLE" << std::endl;

	imc_driver::MultiDevice devices;

	// set verbosity to see outputs on terminal
	devices.setVerbose(true);

	// init devices
	devices.init(ifname, num_devices, timeout);

	// set max torque (needed to allow motion)
	devices.setMaxAllowedTorque(500); // per mil of rated torque

	// configure other parameters
	devices.setProfileAcceleration(600.0); // rad/s^2
	devices.setProfileDeceleration(600.0);
	devices.setMaxAcceleration(600.0);
	devices.setMaxDeceleration(600.0);
	devices.setMaxProfileVelocity(314.0); // rad/s

	// All devices transition to OPERATION_ENABLED
	devices.activate(timeout);

	// switch to velocity mode: send velocity commands for a while and stop
	std::cout << std::endl << "PROFILE_VELOCITY EXAMPLE" << std::endl;
	devices.setOperationMode(imc_driver::OperationMode::PROFILE_VELOCITY);
	std::vector<double> velocity_command(num_devices, 2.0*M_PI); // rad/s
	std::chrono::time_point<std::chrono::system_clock> start_time = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_time = std::chrono::system_clock::now() - start_time;
	while ( elapsed_time.count() <= loop_duration )
	{
		devices.read();
		devices.setTargetVelocity(velocity_command);
		devices.write();
		usleep(loop_period*1000);
		elapsed_time = std::chrono::system_clock::now() - start_time;
	}
	devices.read();	// update last info
	for (unsigned int i = 0; i < num_devices; i++ )
		std::cout << "Actual velocity at device " << i << ": " << devices.getActualVelocity(i) << std::endl;
	velocity_command.assign(num_devices, 0.0);
	devices.setTargetVelocity(velocity_command);
	devices.write();
	devices.read();

	// switch to position mode: absolute to the starting position
	std::cout << std::endl << "PROFILE_POSITION EXAMPLE (ABSOLUTE)" << std::endl;
	for (unsigned int i = 0; i < num_devices; i++ )
		std::cout << "Actual position at device " << i << " (before): " << devices.getActualPosition(i) << std::endl;
	devices.setOperationMode(imc_driver::OperationMode::PROFILE_POSITION);
	devices.setTargetPositionRelative(false); // the position command will be absolute with respect to the power-up position
	devices.setPositionChangeSetImmediately(true); // the drive will start to move to the next position, even if the current one was not reached
	devices.setMaxProfileVelocity(4*M_PI); // limit the velocity to PI rad/s
	std::vector<double> position_command(num_devices);
	for (unsigned int i = 0; i < num_devices; i++ )
		position_command.at(i) = devices.getActualPosition(i)-2*M_PI; // set command one revolution back from the current position
	while ( !devices.allSetPointAcknowledged() ) // blocked here until all devices acknowledge the new set point
	{
		devices.read();
		devices.setTargetPosition(position_command); // update your target position according to control law or whatever
		devices.write();
		usleep(loop_period*1000);
	}
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	while ( !devices.allTargetsReached() && (elapsed_time.count() <= loop_duration) )
	{
		devices.read();
		devices.setTargetPosition(position_command);
		devices.write();
		usleep(loop_period*1000);
		elapsed_time = std::chrono::system_clock::now() - start_time;
	}
	devices.read(); // update after target reached or timeout
	for (unsigned int i = 0; i < num_devices; i++ )
		std::cout << "Actual position at device " << i << " (after): " << devices.getActualPosition(i) << std::endl;

	std::cout << std::endl << "PROFILE_POSITION EXAMPLE (RELATIVE)" << std::endl;
	for (unsigned int i = 0; i < num_devices; i++ )
		std::cout << "Actual position at device " << i << " (before): " << devices.getActualPosition(i) << std::endl;
	devices.setTargetPositionRelative(true); // the position command will be relative
	position_command.assign(num_devices, M_PI); // half a turn
	devices.latchRelativePositionTarget(); // this triggers the start of the relative motion
	while ( !devices.allSetPointAcknowledged() ) // blocked here until all devices acknowledge the new set point
	{
		devices.read();
		devices.setTargetPosition(position_command);
		devices.write();
		usleep(loop_period*1000);
	}
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	while ( !devices.allTargetsReached() && (elapsed_time.count() <= loop_duration) )
	{
		devices.read();
		devices.write();
		usleep(loop_period*1000);
		elapsed_time = std::chrono::system_clock::now() - start_time;
	}
	devices.read(); // update after target reached or timeout
	for (unsigned int i = 0; i < num_devices; i++ )
		std::cout << "Actual position at device " << i << " (after 1): " << devices.getActualPosition(i) << std::endl;
	devices.latchRelativePositionTarget(); // this triggers again the relative motion (new half a turn)
	while ( !devices.allSetPointAcknowledged() ) // blocked here until all devices acknowledge the new set point
	{
		devices.read();
		devices.setTargetPosition(position_command);
		devices.write();
		usleep(loop_period*1000);
	}
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	while ( !devices.allTargetsReached() && (elapsed_time.count() <= loop_duration) )
	{
		devices.read();
		devices.write();
		usleep(loop_period*1000);
		elapsed_time = std::chrono::system_clock::now() - start_time;
	}
	devices.read(); // update after target reached or timeout
	for (unsigned int i = 0; i < num_devices; i++ )
		std::cout << "Actual position at device " << i << " (after 2): " << devices.getActualPosition(i) << std::endl;

	// switch to torque mode: send torque commands for a while and stop
	std::cout << std::endl << "PROFILE_TORQUE EXAMPLE" << std::endl;
	devices.setOperationMode(imc_driver::OperationMode::PROFILE_TORQUE);
	std::vector<double> torque_command(num_devices, 0.1); // Nm
	start_time = std::chrono::system_clock::now();
	elapsed_time = std::chrono::system_clock::now() - start_time;
	while ( elapsed_time.count() <= loop_duration )
	{
		devices.read();
		devices.setTargetTorque(torque_command);
		devices.write();
		usleep(loop_period*1000);
		elapsed_time = std::chrono::system_clock::now() - start_time;
	}
	devices.read(); // update info
	for (unsigned int i = 0; i < num_devices; i++ )
		std::cout << "Actual torque at device " << i << ": " << devices.getActualTorque(i) << std::endl;
	torque_command.assign(num_devices, 0.0);
	devices.setTargetTorque(torque_command);
	devices.write();
	devices.read();

	// check state machine state
	std::cout << std::endl << "CLOSING EXAMPLE" << std::endl;
	for (unsigned int i = 0; i < num_devices; i++ )
		std::cout << "Device " << i << " state: " << devices.getStateMachineState(i) << std::endl;

	devices.deactivate(timeout);
	devices.close();

	return 0;
}
