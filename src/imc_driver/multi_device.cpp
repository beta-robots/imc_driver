#include <imc_driver/multi_device.h>

namespace imc_driver
{

MultiDevice::MultiDevice():verbose__(false),thread_verbose__(false){}
MultiDevice::~MultiDevice(){}

bool MultiDevice::init(const std::string & __if_name, const unsigned int & __num_devices, const double & __timeout_seconds)
{
	if ( __num_devices <= 0 )
	{
		if ( verbose__ )
			std::cout << "ERROR\tInvalid number of devices" << std::endl;
		return false;
	}

	if ( !initEthercat(__if_name, __num_devices) )
	{
		if ( verbose__ )
			std::cout << "ERROR\t Not able to connect to the " << __if_name << " ethernet interface" << std::endl;
		return false;
	}
	if ( verbose__ )
	{
		std::cout << "OK\tEthercat init and config on "<< __if_name << " succeeded" << std::endl;
		std::cout << "\t\tDevices count: " << ec_slavecount << std::endl;
		for ( int i = 0; i < ec_slavecount; i++ )
			std::cout << "\t\tDevice " << i << " name: " << ec_slave[i+1].name << std::endl;
	}

	if ( !configureNetworkMemoryMap() )
	{
		if ( verbose__ )
			std::cout << "ERROR\tError configuring network memory map" << std::endl;
		return false;
	}
	if ( verbose__ )
		std::cout << "OK\tNetwork memory map configured" << std::endl;

	if ( !enableEthercatOperationalState() )
	{
		if ( verbose__ )
			std::cout << "ERROR\tError trying to reach operational state at ethercat state machine" << std::endl;
		return false;
	}
	if ( verbose__ )
		std::cout << "OK\tAll devices reached ethercat operational state" << std::endl;

	allocateData(ec_slavecount);

	// start a thread checking ethercat network
	etherchat_communications_check_thread__.reset(new std::thread(&MultiDevice::ethercatCheck, this));

	readParamsFromDevices();
	if ( verbose__ )
		std::cout << "OK\tParameters read from devices" << std::endl;

	// get data from devices before proceeding
	codeFrame();
	ec_send_processdata();
	ec_receive_processdata(EC_TIMEOUTRET*10);
	decodeFrame();

	if ( !resetFaultDevices(__timeout_seconds) )
	{
		if ( verbose__ )
			std::cout << "ERROR\tUnable to reset all fault devices" << std::endl;
		return false;
	}
	if ( verbose__ )
		std::cout << "OK\tAll faults cleared" << std::endl;

	updateMultiDeviceState();
	multi_device_command__ = MultiDeviceCommand::ENABLE;

	return true;
}

bool MultiDevice::close()
{
	ethercat_in_operation_mutex__.lock();
	in_operation__ = false;
	ethercat_in_operation_mutex__.unlock();
	etherchat_communications_check_thread__->join();
	ec_slave[0].state = EC_STATE_INIT;
	ec_writestate(0); // request INIT ethercat state for all slaves
	ec_close(); // stop SOEM, close socket
	if ( verbose__ )
		std::cout << "OK\tEthercat interface closed" << std::endl;
	return true;
}

bool MultiDevice::activate(const double & __timeout_seconds)
{
	// move to ready to switch on
	for(int i = 0; i<ec_slavecount ; i++)
		command__.at(i) = StateMachineCommand::SHUTDOWN;
	codeFrame();
	std::vector<StateMachineState> expected_states;
	expected_states.assign(ec_slavecount, StateMachineState::READY_TO_SWITCH_ON);
	if ( !sendAndWaitForStates(expected_states, __timeout_seconds/2.0) )
	{
		if ( verbose__ )
			std::cout << "ERROR\tUnable to put devices in ready to switch on state" << std::endl;
		return false;
	}
	if ( verbose__ )
		std::cout << "OK\tAll devices ready to switch on" << std::endl;

	// move to operation enabled
	for(int i = 0; i<ec_slavecount ; i++)
		command__.at(i) = StateMachineCommand::SWITCH_ON_PLUS_ENABLE_OPERATION;
	codeFrame();
	expected_states.assign(ec_slavecount, StateMachineState::OPERATION_ENABLED);
	if ( !sendAndWaitForStates(expected_states, __timeout_seconds/2.0) )
	{
		if ( verbose__ )
			std::cout << "ERROR\tUnable to put devices in operation enabled state" << std::endl;
		return false;
	}
	if ( verbose__ )
		std::cout << "OK\tAll devices enabled" << std::endl;

	updateMultiDeviceState();
	multi_device_command__ = MultiDeviceCommand::ENABLE;

	return true;
}

bool MultiDevice::deactivate(const double & __timeout_seconds)
{
	for(int i = 0; i<ec_slavecount ; i++)
		command__.at(i) = StateMachineCommand::DISABLE_VOLTAGE;
	codeFrame();
	std::vector<StateMachineState> expected_states;
	expected_states.assign(ec_slavecount, StateMachineState::SWITCH_ON_DISABLED);
	if ( !sendAndWaitForStates(expected_states, __timeout_seconds) )
	{
		if ( verbose__ )
			std::cout << "ERROR\tError trying to disable devices" << std::endl;
		return false;
	}
	if ( verbose__ )
		std::cout << "OK\tAll devices disabled" << std::endl;
	return true;
}

void MultiDevice::write()
{
	switch ( multi_device_state__ )
	{
		case ENABLED:
			switch ( multi_device_command__ )
			{
				case ENABLE:
					break;
				case REARM:
					multi_device_command__ = MultiDeviceCommand::ENABLE;	// reset rearm flag
					break;
				case DISABLE:
					command__.assign(ec_slavecount, StateMachineCommand::DISABLE_VOLTAGE);
					break;
				case EMERGENCY_STOP:
				default:
					command__.assign(ec_slavecount, StateMachineCommand::QUICK_STOP);
					break;
			}
			break;

		case DISABLED:
			switch ( multi_device_command__ )
			{
				case ENABLE:
				case REARM:
					commandSequenceToEnable();
					break;
				case DISABLE:
					command__.assign(ec_slavecount, StateMachineCommand::DISABLE_VOLTAGE);
					break;
				case EMERGENCY_STOP:
				default:
					command__.assign(ec_slavecount, StateMachineCommand::QUICK_STOP);
					break;
			}
			break;

		case ERROR:
			switch ( multi_device_command__ )
			{
				case REARM:
					commandSequenceToEnable();
					break;
				case DISABLE:
					command__.assign(ec_slavecount, StateMachineCommand::DISABLE_VOLTAGE);
					break;
				case ENABLE:
				case EMERGENCY_STOP:
				default:
					command__.assign(ec_slavecount, StateMachineCommand::QUICK_STOP);
					break;
			}
			break;

		default:
			command__.assign(ec_slavecount, StateMachineCommand::QUICK_STOP);
			break;
	}
	codeFrame();
	ec_send_processdata();
}

void MultiDevice::read()
{
	wkc_mutex__.lock();
	current_wkc__ = ec_receive_processdata(EC_TIMEOUTRET);
	wkc_mutex__.unlock();
	decodeFrame();
	updateMultiDeviceState();
}

void MultiDevice::updateMultiDeviceState()
{
	if ( someDeviceInFault() )
	{
		multi_device_state__ = MultiDeviceState::ERROR;
	}
	else
	{
		if ( allDevicesInState(StateMachineState::OPERATION_ENABLED) )
			multi_device_state__ = MultiDeviceState::ENABLED;
		else
			multi_device_state__ = MultiDeviceState::DISABLED;
	}
}

void MultiDevice::quickStop()
{
	multi_device_command__ = MultiDeviceCommand::EMERGENCY_STOP;
}

void MultiDevice::enable()
{
	if ( multi_device_state__ == MultiDeviceState::ERROR )
		multi_device_command__ = MultiDeviceCommand::REARM;
	else
		multi_device_command__ = MultiDeviceCommand::ENABLE;
}

void MultiDevice::disable()
{
	multi_device_command__ = MultiDeviceCommand::DISABLE;
}

bool MultiDevice::initEthercat(const std::string & __if_name, const unsigned int & __num_devices)
{
	in_operation__ = false;

	if ( !ec_init(__if_name.c_str()) )
	{
		if ( verbose__ )
			std::cout << "No socket connection on "<< __if_name << ", execute as root" << std::endl;
		return false;
	}

	if ( ec_config_init(FALSE) <= 0)
	{
		if ( verbose__ )
			std::cout << "Error configuring ethercat" << std::endl;
		return false;
	}
	if ( ec_slavecount != __num_devices )
	{
		if ( verbose__ )
			std::cout << "Number of devices mismatch, found: " << ec_slavecount << ", expected: " << __num_devices << std::endl;
		return false;
	}

	return true;
}

bool MultiDevice::configureNetworkMemoryMap()
{
	uint32 data32;
	uint8 data8;
	for ( uint16 i = 0; i < ec_slavecount; i++)
	{
		// define TPDOs (from nodes to master)
		writeSDO<uint32>(i, 0x1A00, 1, 0x60410010);	//Status word, 0x6041 00 16bits
		writeSDO<uint32>(i, 0x1A00, 2, 0x606C0020);	//Velocity, 0x606C 00 32bits
		writeSDO<uint32>(i, 0x1A00, 3, 0x60770010);	//Torque, 0x6077 00 16bits
		writeSDO<uint32>(i, 0x1A00, 4, 0x60640020);	//Position, 0x6064 00 32bits
		writeSDO<uint32>(i, 0x1A00, 5, 0x60610008);	//Operation mode, 0x6061 00 8bits
		writeSDO<uint32>(i, 0x1A00, 6, 0x60F40020);	//Following error, 0x60F4 00 32bits
		writeSDO<uint32>(i, 0x1A00, 7, 0x20C20120);	//Drive actual temperature, 0x20C2 01 32bits
		writeSDO<uint32>(i, 0x1A00, 8, 0x603F0010);	//Error code, 0x603F 00 16bits

		// define RPDOs (from master to nodes)
		/*
		RxPDO4 will be used:
		0x6040 0 16 Control word
		0x607A 0 32 Target position
		0x60FF 0 32 Target velocity
		0x6071 0 16 Target torque
		0x6072 0 16 Max torque
		0x6060 0 8 Mode of operation
		*/
		//TPDO1 is active by default, but RPDO4 is not
		writeSDO<uint32>(i, 0x1C12, 1, 0x1603);	// Set the RPDO4 instead of the RPDO1

		// command reference
		writeSDO<uint8>(i, COMMAND_REFERENCE_SOURCE_IDX, COMMAND_REFERENCE_SOURCE_SUB, 0x00);	//Command reference source set to Network
	}
	int used_memory = ec_config_map(&io_map__);
	if ( used_memory > sizeof(io_map__) )
	{
		if ( verbose__ )
			std::cout << "Memory map configuration not ok, used: " << used_memory << ", max expected: " << sizeof(io_map__) << std::endl;
		return false;
	}
	ec_configdc();

	return true;
}

bool MultiDevice::enableEthercatOperationalState()
{
	// wait for all slaves to reach SAFE_OP state
	if ( ec_statecheck(0, EC_STATE_SAFE_OP,  EC_TIMEOUTSTATE * 4) <= 0 )
	{
		if ( verbose__ )
			std::cout << "Error reaching ethercat SAFE_OP state" << std::endl;
		return false;
	}

	expected_wkc__ = (ec_group[0].outputsWKC * 2) + ec_group[0].inputsWKC;

	// Request operational state for all slaves
	ec_slave[0].state = EC_STATE_OPERATIONAL; // 0 index means master or all devices

	//send-receive one valid process data to make outputs in slaves happy
	ec_send_processdata();
	ec_receive_processdata(EC_TIMEOUTRET);

	//request OP state for all slaves
	ec_writestate(0);

	// wait for all slaves to reach OP state or time out after some trials
	for ( unsigned int trials = 0; trials < 40; trials++)
	{
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		ec_statecheck(0, EC_STATE_OPERATIONAL, 50000);
		if ( ec_slave[0].state == EC_STATE_OPERATIONAL )
			break;
	}
	if ( ec_slave[0].state != EC_STATE_OPERATIONAL )
	{
		if ( verbose__ )
			std::cout << "Not all slaves reached ethercat operational state" << std::endl;
		return false;
	}

	// fill state for each individual slave
	ec_readstate();

	in_operation__ = true;

	return true;
}

void MultiDevice::allocateData(const unsigned int & __num_devices)
{
	// allocate internal variables
	command__.assign(__num_devices, StateMachineCommand::DISABLE_VOLTAGE);
	state_machine_state__.resize(__num_devices);
	current_operation_mode__.resize(__num_devices);
	desired_operation_mode__.resize(__num_devices);
	position_command__.assign(__num_devices, 0.0);
	velocity_command__.assign(__num_devices, 0.0);
	torque_command__.assign(__num_devices, 0.0);
	position_feedback__.assign(__num_devices, 0.0);
	velocity_feedback__.assign(__num_devices, 0.0);
	torque_feedback__.assign(__num_devices, 0.0);
	encoder_increments_per_turn__.assign(__num_devices, 0.0);
	rated_current__.assign(__num_devices, 0.0);
	rated_torque__.assign(__num_devices, 0.0);
	profile_acceleration__.assign(__num_devices, 0.0);
	profile_deceleration__.assign(__num_devices, 0.0);
	max_velocity__.assign(__num_devices, 0.0);
	max_acceleration__.assign(__num_devices, 0.0);
	max_deceleration__.assign(__num_devices, 0.0);
	max_allowed_torque__.assign(__num_devices, 0.0);
	drive_temperature__.resize(__num_devices);
	error_code__.assign(__num_devices, 0x0000);
	following_error__.assign(__num_devices, 0.0);
	target_reached__.resize(__num_devices);
	set_point_acknowledge__.resize(__num_devices);
	ethercat_state__.resize(__num_devices);
	last_position_command__.assign(__num_devices, 0.159756); // avoid init to 0.0
	target_position_relative__.assign(__num_devices, false);
	position_change_set_immediately__.assign(__num_devices, false);
	latch_relative_position_target__.assign(__num_devices, false);
}

void MultiDevice::readParamsFromDevices()
{
	uint32 encoder_increments, motor_revolutions;
	for( unsigned int i = 0; i < ec_slavecount ; i++ )
	{
		// encoder related
		readSDO<uint32>(i, ENCODER_INCREMENTS_IDX, ENCODER_INCREMENTS_SUB, &encoder_increments);
		readSDO<uint32>(i, MOTOR_REVOLUTIONS_IDX, MOTOR_REVOLUTIONS_SUB, &motor_revolutions);
		encoder_increments_per_turn__.at(i) = (double)encoder_increments/(double)motor_revolutions;

		// motor related
		rated_current__.at(i) = getValueFromDevice<double,uint32>(i, MOTOR_RATED_CURRENT_IDX, MOTOR_RATED_CURRENT_SUB, 0.001);	// to [A]
		rated_torque__.at(i) = getValueFromDevice<double,uint32>(i, MOTOR_RATED_TORQUE_IDX, MOTOR_RATED_TORQUE_SUB, 0.001);	// to [Nm]
	}
}

bool MultiDevice::resetFaultDevices(const double & __timeout_seconds)
{
	for(int i = 0; i < ec_slavecount ; i++)
		command__.at(i) = StateMachineCommand::FAULT_RESET_RISE_EDGE;
	codeFrame();
	std::vector<StateMachineState> expected_states;
	expected_states.assign(ec_slavecount, StateMachineState::SWITCH_ON_DISABLED);
	return sendAndWaitForStates(expected_states, __timeout_seconds);
}

bool MultiDevice::sendAndWaitForOperationModes(const std::vector<OperationMode> & __operation_mode, const double & __timeout_seconds)
{
	std::chrono::time_point<std::chrono::system_clock> start_time = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_time = std::chrono::system_clock::now() - start_time;
	bool all_ok = false;
	while ( (elapsed_time.count() <= __timeout_seconds) && (!all_ok) )
	{
		all_ok = true;
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		decodeFrame();	// updates operation modes
		for(int i = 0; i < ec_slavecount ; i++)
			if ( current_operation_mode__.at(i) != __operation_mode.at(i) )
				all_ok = false;
		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	if ( all_ok )
		return true;
	else
		return false;
}

bool MultiDevice::sendAndWaitForStates(const std::vector<StateMachineState> & __expected_state, const double & __timeout_seconds)
{
	std::chrono::time_point<std::chrono::system_clock> start_time = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_time = std::chrono::system_clock::now() - start_time;
	bool all_ok = false;
	while ( (elapsed_time.count() <= __timeout_seconds) && (!all_ok) )
	{
		all_ok = true;
		ec_send_processdata();
		ec_receive_processdata(EC_TIMEOUTRET);
		decodeFrame();	// updates state machine states
		for(int i = 0; i < ec_slavecount ; i++)
			if ( state_machine_state__.at(i) != __expected_state.at(i) )
				all_ok = false;
		elapsed_time = std::chrono::system_clock::now() - start_time;
		usleep(10000);
	}
	if ( all_ok )
		return true;
	else
		return false;
}

void MultiDevice::commandSequenceToEnable()
{
	for (unsigned int i = 0; i < ec_slavecount; i++)
	{
		switch ( state_machine_state__.at(i) )
		{
			case StateMachineState::SWITCH_ON_DISABLED:
				command__.at(i) = StateMachineCommand::SHUTDOWN;
				break;
			case StateMachineState::READY_TO_SWITCH_ON:
				command__.at(i) = StateMachineCommand::SWITCH_ON_PLUS_ENABLE_OPERATION;
				break;
			case StateMachineState::SWITCHED_ON:
			case StateMachineState::OPERATION_ENABLED:
			case StateMachineState::QUICK_STOP_ACTIVE:
				command__.at(i) = StateMachineCommand::ENABLE_OPERATION;
				break;
			case StateMachineState::FAULT:
				if ( command__.at(i) == StateMachineCommand::FAULT_RESET_RISE_EDGE )
					command__.at(i) = StateMachineCommand::FAULT_RESET_FALL_EDGE;
				else
					command__.at(i) = StateMachineCommand::FAULT_RESET_RISE_EDGE;
				break;
			default:
				command__.at(i) = StateMachineCommand::DISABLE_VOLTAGE;
				break;
		}
	}
}

bool MultiDevice::allDevicesInState(const StateMachineState & __state_machine_state)
{
	for(unsigned int i = 0; i < ec_slavecount; i++)
		if ( state_machine_state__.at(i) != __state_machine_state )
			return false;
	return true;
}

bool MultiDevice::someDeviceInFault()
{
	for(unsigned int i = 0; i < ec_slavecount; i++)
		if ( state_machine_state__.at(i) == StateMachineState::FAULT )
			return true;
	return false;
}

void MultiDevice::codeFrame()
{
	uint16 command;
	for(int i = 0; i < ec_slavecount ; i++)
	{
		// control command
		switch ( current_operation_mode__.at(i) )
		{
			case OperationMode::PROFILE_POSITION:
			{
				int32 target_position = (int32)( position_command__.at(i) * encoder_increments_per_turn__.at(i) / ( 2.0 * M_PI) );
				*(ec_slave[i+1].outputs+2) = target_position & 0x000000FF;
				*(ec_slave[i+1].outputs+3) = (target_position >> 8) & 0x000000FF;
				*(ec_slave[i+1].outputs+4) = (target_position >> 16) & 0x000000FF;
				*(ec_slave[i+1].outputs+5) = (target_position >> 24) & 0x000000FF;
				break;
			}
			case OperationMode::PROFILE_VELOCITY:
			{
				int32 target_velocity_encoder = (int32)( velocity_command__.at(i) * encoder_increments_per_turn__.at(i) / ( 2.0 * M_PI) );
				*(ec_slave[i+1].outputs+6) = target_velocity_encoder & 0x000000FF;
				*(ec_slave[i+1].outputs+7) = (target_velocity_encoder >> 8) & 0x000000FF;
				*(ec_slave[i+1].outputs+8) = (target_velocity_encoder >> 16) & 0x000000FF;
				*(ec_slave[i+1].outputs+9) = (target_velocity_encoder >> 24) & 0x000000FF;
				break;
			}
			case OperationMode::PROFILE_TORQUE:
			{
				int32 target_torque_encoder = (int32)( torque_command__.at(i) * 1000.0 / rated_torque__.at(i) );
				*(ec_slave[i+1].outputs+10) = target_torque_encoder & 0x00FF;
				*(ec_slave[i+1].outputs+11) = (target_torque_encoder >> 8) & 0x00FF;
				break;
			}
			default:
				break;
		}

		command = stateMachineCommand2ControlWordCommand(command__.at(i));
		if ( current_operation_mode__.at(i) == OperationMode::PROFILE_POSITION )
		{
			if ( target_position_relative__.at(i) )
			{
				command = command | 0x0040;
				if ( latch_relative_position_target__.at(i) )
				{
					command = command | 0x0010; // target latched -> new set point
					latch_relative_position_target__.at(i) = false;
				}
			}
			else // absolute mode, new set point if command is different from previous
			{
				if ( last_position_command__.at(i) != position_command__.at(i) )
					command = command | 0x0010;
			}
			last_position_command__.at(i) = position_command__.at(i);
			if ( position_change_set_immediately__.at(i) )
				command = command | 0x0020;
		}
		*(ec_slave[i+1].outputs) = command & 0x00FF;
		*(ec_slave[i+1].outputs+1) = command >> 8;

		// max torque
		*(ec_slave[i+1].outputs+12) = max_allowed_torque__.at(i) & 0x00FF;
		*(ec_slave[i+1].outputs+13) = max_allowed_torque__.at(i) >> 8;

		// operation mode
		*(ec_slave[i+1].outputs+14) = desired_operation_mode__.at(i);
	}
}

void MultiDevice::decodeFrame()
{
	// decode received data
	int32 actual_position;
	int32 actual_velocity;
	int16 actual_torque;
	int32 following_error;
	int32 temperature;

	for( unsigned int i = 0; i < ec_slavecount; i++ )
	{
		// motor position, [rad]
		actual_position = *(ec_slave[i+1].inputs + 11) << 24 | *(ec_slave[i+1].inputs + 10) << 16 | *(ec_slave[i+1].inputs + 9) << 8 | *(ec_slave[i+1].inputs + 8);
		position_feedback__.at(i) = ( (double)actual_position ) * 2.0 * M_PI / encoder_increments_per_turn__.at(i);

		// motor velocity, [rad/s]
		actual_velocity = *(ec_slave[i+1].inputs + 5) << 24 | *(ec_slave[i+1].inputs + 4) << 16 | *(ec_slave[i+1].inputs + 3) << 8 | *(ec_slave[i+1].inputs + 2);
		velocity_feedback__.at(i) = ( (double)actual_velocity ) * 2.0 * M_PI / encoder_increments_per_turn__.at(i);

		// motor torque, [Nm]
		actual_torque = *(ec_slave[i+1].inputs + 7) << 8 | *(ec_slave[i+1].inputs + 6);
		torque_feedback__.at(i) = ( (double)actual_torque ) * rated_torque__.at(i) / 1000.0;

		// operation mode
		current_operation_mode__.at(i) = (OperationMode)*(ec_slave[i+1].inputs + 12);

		// state machine state
		uint16 status_word = *(ec_slave[i+1].inputs + 1) << 8 | *(ec_slave[i+1].inputs + 0);
		state_machine_state__.at(i) = statusWord2StateMachineState(status_word);
		// check bit 10 for target reached
		target_reached__.at(i) = ( (status_word & 0x0400) == 0x0400 );

		// check bit 12 for set point acknowledge
		set_point_acknowledge__.at(i) = ( (status_word & 0x1000) == 0x1000 );

		// following error [rad]
		following_error = *(ec_slave[i+1].inputs + 16) << 24 | *(ec_slave[i+1].inputs + 15) << 16 | *(ec_slave[i+1].inputs + 14) << 8 | *(ec_slave[i+1].inputs + 13);
		following_error__.at(i) = ( (double)following_error ) * 2.0 * M_PI / encoder_increments_per_turn__.at(i);

		// drive temperature
		temperature = *(ec_slave[i+1].inputs + 20) << 24 | *(ec_slave[i+1].inputs + 19) << 16 | *(ec_slave[i+1].inputs + 18) << 8 | *(ec_slave[i+1].inputs + 17);
		drive_temperature__.at(i) = (double)(temperature) / 1000.0;

		// error code
		error_code__.at(i) = *(ec_slave[i+1].inputs + 22) << 8 | *(ec_slave[i+1].inputs + 21);
	}
}

StateMachineState MultiDevice::statusWord2StateMachineState(const uint16 & __status_word)
{
	switch ( __status_word & 0x006F )
	{
		case 0x00:
		case 0x20:
			return NOT_READY_TO_SWITCH_ON;
			break;
		case 0x40:
		case 0x60:
			return SWITCH_ON_DISABLED;
			break;
		case 0x21:
			return READY_TO_SWITCH_ON;
			break;
		case 0x23:
			return SWITCHED_ON;
			break;
		case 0x27:
			return OPERATION_ENABLED;
			break;
		case 0x07:
			return QUICK_STOP_ACTIVE;
			break;
		case 0x0F:
		case 0x2F:
			return FAULT_REACTION_ACTIVE;
			break;
		case 0x08:
		case 0x28:
			return FAULT;
			break;
		default:
			return UNKNOWN;
			break;
	}
}

uint16 MultiDevice::stateMachineCommand2ControlWordCommand(const StateMachineCommand & __state_machine_command)
{
	switch ( __state_machine_command )
	{
		case SHUTDOWN:
			return 0x0006;
			break;
		case SWITCH_ON:
			return 0x0007;
			break;
		case SWITCH_ON_PLUS_ENABLE_OPERATION:
			return 0x000F;
			break;
		case DISABLE_VOLTAGE:
			return 0x0000;
			break;
		case QUICK_STOP:
			return 0x0002;
			break;
		case DISABLE_OPERATION:
			return 0x0007;
			break;
		case ENABLE_OPERATION:
			return 0x000F;
			break;
		case FAULT_RESET_RISE_EDGE:
			return 0x0080;
			break;
		case FAULT_RESET_FALL_EDGE:
			return 0x0000;
			break;
		default:
			return 0x0000;
			break;
	}
}

std::string MultiDevice::StateMachineState2String(const StateMachineState & __state_machine_state)
{
	switch ( __state_machine_state )
	{
		case NOT_READY_TO_SWITCH_ON:
			return "NOT_READY_TO_SWITCH_ON";
			break;
		case SWITCH_ON_DISABLED:
			return "SWITCH_ON_DISABLED";
			break;
		case READY_TO_SWITCH_ON:
			return "READY_TO_SWITCH_ON";
			break;
		case OPERATION_ENABLED:
			return "OPERATION_ENABLED";
			break;
		case SWITCHED_ON:
			return "SWITCHED_ON";
			break;
		case QUICK_STOP_ACTIVE:
			return "QUICK_STOP_ACTIVE";
			break;
		case FAULT_REACTION_ACTIVE:
			return "FAULT_REACTION_ACTIVE";
			break;
		case FAULT:
			return "FAULT";
			break;
		default:
			return "UNKNOWN";
			break;
	}
}

bool MultiDevice::latchRelativePositionTarget(const unsigned int & __device_number)
{
	if ( !checkDeviceNumber(__device_number) )
		return false;
	latch_relative_position_target__.at(__device_number) = true;
	return true;
}
void MultiDevice::latchRelativePositionTarget()
{
	latch_relative_position_target__.assign(ec_slavecount, true);
}

void MultiDevice::printLastError()
{
	for (unsigned int i = 0; i < ec_slavecount; i++)
	{
		uint16 error = getLastErrorCode(i);
		if ( error != 0x0000 )
			std::cout << "ERROR\tDevice " << i << ": " << errorCode2String(error) << " (0x" << std::hex << std::setfill('0') << std::setw(4) << error << ")" << std::dec << std::endl;
	}
}

void MultiDevice::ethercatCheck()
{
	int slave;
	uint8 current_group = 0;

	thread_verbose_mutex__.lock();
	if ( thread_verbose__ )
		std::cout << "Entering thread" << std::endl;
	thread_verbose_mutex__.unlock();
	while(1)
	{
		ethercat_in_operation_mutex__.lock();
		if ( !in_operation__ )
			break;
		ethercat_in_operation_mutex__.unlock();

		wkc_mutex__.lock();
		bool wkc_check_fail = (current_wkc__ < expected_wkc__);
		wkc_mutex__.unlock();

		if( wkc_check_fail || ec_group[current_group].docheckstate )
		{
			/* one ore more slaves are not responding */
			ec_group[current_group].docheckstate = FALSE;
			ec_readstate();

			ethercat_state_mutex__.lock();
			for ( slave = 1; slave <= ec_slavecount; slave++ )
				ethercat_state__.at(slave-1) = ec_slave[slave].state;
			ethercat_state_mutex__.unlock();

			for (slave = 1; slave <= ec_slavecount; slave++)
			{
				if ((ec_slave[slave].group == current_group) && (ec_slave[slave].state != EC_STATE_OPERATIONAL))
				{
					ec_group[current_group].docheckstate = TRUE;
					if( ec_slave[slave].state == (EC_STATE_SAFE_OP + EC_STATE_ERROR) )
					{
						thread_verbose_mutex__.lock();
						if ( thread_verbose__ )
							std::cout << "ERROR : slave "<< slave << " is in SAFE_OP + ERROR, attempting ack" << std::endl;
						thread_verbose_mutex__.unlock();
						ec_slave[slave].state = (EC_STATE_SAFE_OP + EC_STATE_ACK);
						ec_writestate(slave);
					}
					else if( ec_slave[slave].state == EC_STATE_SAFE_OP )
					{
						thread_verbose_mutex__.lock();
						if ( thread_verbose__ )
							std::cout << "WARNING : slave " << slave << " is in SAFE_OP, change to OPERATIONAL." << std::endl;
						thread_verbose_mutex__.unlock();
						ec_slave[slave].state = EC_STATE_OPERATIONAL;
						ec_writestate(slave);
					}
					else if( ec_slave[slave].state > EC_STATE_NONE )
					{
						if (ec_reconfig_slave(slave, EC_TIMEOUTMON))
						{
							ec_slave[slave].islost = FALSE;
							thread_verbose_mutex__.lock();
							if ( thread_verbose__ )
								std::cout << "MESSAGE : slave " << slave << " reconfigured" << std::endl;
							thread_verbose_mutex__.unlock();
						}
					}
					else if( !ec_slave[slave].islost )
					{
						/* re-check state */
						ec_statecheck(slave, EC_STATE_OPERATIONAL, EC_TIMEOUTRET);
						if( ec_slave[slave].state == EC_STATE_NONE )
						{
							ec_slave[slave].islost = TRUE;
							thread_verbose_mutex__.lock();
							if ( thread_verbose__ )
								std::cout << "ERROR : slave " << slave << " lost" << std::endl;
							thread_verbose_mutex__.unlock();
						}
					}
				}
				if( ec_slave[slave].islost )
				{
					if( ec_slave[slave].state == EC_STATE_NONE )
					{
						if (ec_recover_slave(slave, EC_TIMEOUTMON))
						{
							ec_slave[slave].islost = FALSE;
							thread_verbose_mutex__.lock();
							if ( thread_verbose__ )
								std::cout << "MESSAGE : slave " << slave << " recovered" << std::endl;
							thread_verbose_mutex__.unlock();
						}
					}
					else
					{
						ec_slave[slave].islost = FALSE;
						thread_verbose_mutex__.lock();
						if ( thread_verbose__ )
							std::cout << "MESSAGE : slave " << slave << " found" << std::endl;
						thread_verbose_mutex__.unlock();
					}
				}
			}
			//if( !ec_group[current_group].docheckstate )
				//std::cout << "OK : all slaves resumed OPERATIONAL." << std::endl; Entropy generator
		}
		usleep(10000);
	}
	ethercat_in_operation_mutex__.unlock();
}

}
