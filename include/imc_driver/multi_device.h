#ifndef ___IMC_DRIVER___MULTI_DEVICE_H___
#define ___IMC_DRIVER___MULTI_DEVICE_H___

#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <unistd.h>
#include <thread>
#include <cmath>
#include <vector>
#include <mutex>

#include "soem/ethercattype.h"
#include "soem/nicdrv.h"
#include "soem/ethercatbase.h"
#include "soem/ethercatmain.h"
#include "soem/ethercatdc.h"
#include "soem/ethercatcoe.h"
#include "soem/ethercatfoe.h"
#include "soem/ethercatconfig.h"
#include "soem/ethercatprint.h"
#include "soem/osal.h"

#include <imc_driver/constants.h>
#include <imc_driver/registry_indexes.h>

namespace imc_driver
{

/*! \class MultiDevice
	\brief MultiDevice class uses the soem EtherCAT library to provide simultaneous communication with several imc_drives.
*/
class MultiDevice
{

public:

	//! Position command.
	/*! Desired position for all devices in [rad]. */
	std::vector<double> position_command__;

	//! Velocity command.
	/*! Desired speed for all devices in [rad/s]. */
	std::vector<double> velocity_command__;

	//! Torque command.
	/*! Desired torque for all devices in [Nm]. */
	std::vector<double> torque_command__;

	//! Position feedback.
	/*! Actual motor position of all devices in [rad]. */
	std::vector<double> position_feedback__;

	//! Velocity feedback.
	/*! Actual motor speed of all devices in [rad/s]. */
	std::vector<double> velocity_feedback__;

	//! Torque feedback.
	/*! Actual motor torque of all devices in [Nm]. */
	std::vector<double> torque_feedback__;

private:

	MultiDeviceState multi_device_state__;
	MultiDeviceCommand multi_device_command__;

	// network memory map
	char io_map__[4096];

	// ethercat thread
	std::shared_ptr<std::thread> etherchat_communications_check_thread__;
	bool in_operation__;
	int expected_wkc__; //expected working counter
	int current_wkc__;
	std::vector<uint16> ethercat_state__;
	std::mutex ethercat_state_mutex__;
	std::mutex ethercat_in_operation_mutex__;
	std::mutex wkc_mutex__;

	// to mount control word
	std::vector<OperationMode> current_operation_mode__;
	std::vector<OperationMode> desired_operation_mode__;
	std::vector<StateMachineState> state_machine_state__;
	std::vector<StateMachineCommand> command__;
	std::vector<double> last_position_command__;
	std::vector<bool> target_position_relative__;
	std::vector<bool> position_change_set_immediately__;
	std::vector<bool> latch_relative_position_target__;

	// motion data
	std::vector<double> following_error__;
	std::vector<double> drive_temperature__;
	std::vector<uint16> error_code__;
	std::vector<bool> target_reached__;
	std::vector<bool> set_point_acknowledge__;

	// data from devices
	std::vector<double> encoder_increments_per_turn__;
	std::vector<double> rated_current__;
	std::vector<double> rated_torque__;

	// configuration parameters
	std::vector<int16> max_allowed_torque__;
	std::vector<double> profile_acceleration__;
	std::vector<double> profile_deceleration__;
	std::vector<double> max_velocity__;
	std::vector<double> max_acceleration__;
	std::vector<double> max_deceleration__;

	// to enable/disable standard outputs
	bool verbose__;
	bool thread_verbose__;
	std::mutex thread_verbose_mutex__;

public:

	//! Constructor.
	MultiDevice();

	//! Destructor.
	~MultiDevice();

	//! Initializes variables,
	//! attempts connection through given interface,
	//! configures the network,
	//! starts a thread to maintain the ethercat state machine of all devices,
	//! reads some params from devices,
	//! and attempts to reset devices in ::FAULT.
	/*!
		\param __if_name name of the ethernet interface.
		\param __num_devices the number of imc_drives connected to the network.
		\param __timeout_seconds timeout to stop attempting to reset fault devices in [s].
		\return false if some step failed, true otherwise.
	*/
	bool init(const std::string & __if_name, const unsigned int & __num_devices, const double & __timeout_seconds);

	//! Attempts to transition all devices to the ::OPERATION_ENABLED state.
	/*! This method should be called outside the user's read/write loop, since a communication loop is established internally with the devices until success or timeout.
		\param __timeout_seconds timeout to stop attempting the transition in [s].
		\return true if all devices reach ::OPERATION_ENABLED state before timeout, false otherwise.
	*/
	bool activate(const double & __timeout_seconds);

	//! Attempts to transition all devices to the ::SWITCH_ON_DISABLED state.
	/*! This method should be called outside the user's read/write loop, since a communication loop is established internally with the devices until success or timeout.
		\param __timeout_seconds timeout to stop attempting the transition in [s].
		\return true if all devices reach ::SWITCH_ON_DISABLED state before timeout, false otherwise.
	*/
	bool deactivate(const double & __timeout_seconds);

	//! Closes EtherCAT communications and disconnects from the network.
	/*!
		For a clean exit, make sure to call imc_driver::MultiDevice::deactivate before calling imc_driver::MultiDevice::close.
		\return true.
	*/
	bool close();

	//! Codes the EtherCAT frame using current imc_driver::MultiDeviceState, settings, and requests,
	//! and sends it through the network.
	void write();

	//! Attempts to receive data from the network,
	//! decodes the EtherCAT frame, and updates the current imc_driver::MultiDeviceState.
	void read();

	//! Requests a quick stop for all devices.
	void quickStop();

	//! Requests to transition all devices to the ::OPERATION_ENABLED state.
	void enable();

	//! Requests to disable voltage on all devices.
	void disable();

	//! Sets verbosity preference.
	/*!
		\param __verbose true to enable standard outputs, false to disable.
	*/
	void setVerbose(const bool __verbose);

	//! Checks if all devices reached their target.
	/*!
		\return true if all devices reached their target, false otherwise.
	*/
	bool allTargetsReached();

	//! Checks if all devices acknowledge the position set point.
	/*!
		\return true if all devices acknowledge the position set point.
	*/
	bool allSetPointAcknowledged();

	//! ::MultiDeviceState accessor
	/*!
		\return The current ::MultiDeviceState.
	*/
	MultiDeviceState getMultiDeviceState();

	//! Current ::OperationMode accessor.
	/*!
		\param __device_number Device number.
		\return The ::OperationMode.
	*/
	OperationMode getOperationMode(const unsigned int & __device_number);

	//! Current ::OperationMode accessor.
	/*!
		\return The ::OperationMode at each device.
	*/
	std::vector<OperationMode> getOperationMode();

	//! ::OperationMode mutator.
	/*! The change of operation mode will actually be requested to the devices on the next call to MultiDevice::write.
		\param __operation_mode desired operation mode.
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setOperationMode(const OperationMode & __operation_mode, const unsigned int & __device_number);

	//! ::OperationMode mutator.
	/*! The change of operation mode will actually be requested to the devices on the next call to MultiDevice::write.
		\param __operation_modes desired operation mode for each device.
		\return false if size of \p __operation_modes does not match the number of devices, true otherwise.
	*/
	bool setOperationMode(const std::vector<OperationMode> & __operation_modes);

	//! ::OperationMode mutator.
	/*! The change of operation mode will actually be requested to the devices on the next call to MultiDevice::write.
		Sets the same value for all devices.
		\param __operation_mode desired operation mode for all devices.
	*/
	void setOperationMode(const OperationMode & __operation_mode);

	//! Current ::StateMachineState accessor.
	/*!
		\param __device_number Device number.
		\return The ::StateMachineState.
	*/
	StateMachineState getStateMachineState(const unsigned int & __device_number);

	//! Current ::StateMachineState accessor.
	/*!
		\return The ::StateMachineState at each device.
	*/
	std::vector<StateMachineState> getStateMachineState();

	//! Current ::StateMachineState accessor in readable form.
	/*!
		\param __device_number Device number.
		\return A string representing the ::StateMachineState.
	*/
	std::string getStateMachineStateAsString(const unsigned int & __device_number);

	//! Current ::StateMachineState accessor in readable form.
	/*!
		\return A string representing the ::StateMachineState for each device.
	*/
	std::vector<std::string> getStateMachineStateAsString();

	//! Current EtherCAT state machine state accessor.
	/*!
		\param __device_number Device number.
		\return The EtherCAT state machine state as given by the soem library.
	*/
	uint16 getEthercatState(const unsigned int & __device_number);

	//! Current EtherCAT state machine state accessor.
	/*!
		\return The EtherCAT state machine state as given by the soem library for each device.
	*/
	std::vector<uint16> getEthercatState();

	//! Rated torque accessor.
	/*!
		\param __device_number Device number.
		\return The rated torque in [Nm].
	*/
	double getRatedTorque(const unsigned int & __device_number);

	//! Rated torque accessor.
	/*!
		\return The rated torque in [Nm] for each device.
	*/
	std::vector<double> getRatedTorque();

	//! Actual velocity accessor.
	/*!
		\param __device_number Device number.
		\return The actual device's axis speed in [rad/s].
	*/
	double getActualVelocity(const unsigned int & __device_number);

	//! Actual velocity accessor.
	/*!
		\return The actual axis speed in [rad/s] at each device.
	*/
	std::vector<double> getActualVelocity();

	//! Actual torque accessor.
	/*!
		\param __device_number Device number.
		\return The actual device's torque in [Nm].
	*/
	double getActualTorque(const unsigned int & __device_number);

	//! Actual torque accessor.
	/*!
		\return The actual torque in [Nm] at each device.
	*/
	std::vector<double> getActualTorque();

	//! Actual position accessor.
	/*!
		\param __device_number Device number.
		\return The actual device's axis position in [rad].
	*/
	double getActualPosition(const unsigned int & __device_number);

	//! Actual position accessor.
	/*!
		\return The actual axis position in [rad] at each device.
	*/
	std::vector<double> getActualPosition();

	//! Actual following error accessor.
	/*!
		\param __device_number Device number.
		\return The actual device's following error in [rad].
	*/
	double getActualFollowingError(const unsigned int & __device_number);

	//! Actual following error accessor.
	/*!
		\return The actual following error in [rad] at each device.
	*/
	std::vector<double> getActualFollowingError();

	//! Drive temperature accessor.
	/*!
		\param __device_number Device number.
		\return The current drive temperature in [ºC].
	*/
	double getDriveTemperature(const unsigned int & __device_number);

	//! Drive temperature accessor.
	/*!
		\return The current temperature in [ºC] at each drive.
	*/
	std::vector<double> getDriveTemperature();

	//! Supported drive modes accessor.
	/*!
		\param __device_number Device number.
		\return The device's supported ::OperationMode.
	*/
	std::vector<OperationMode> getSupportedDriveModes(const unsigned int & __device_number);

	//! Supported drive modes accessor.
	/*!
		\return The supported ::OperationMode at each device.
	*/
	std::vector<std::vector<OperationMode>> getSupportedDriveModes();

	//! Profile acceleration accessor.
	/*!
		\param __device_number Device number.
		\return The profile acceleration in [rad/s^2].
	*/
	double getProfileAcceleration(const unsigned int & __device_number);

	//! Profile acceleration accessor.
	/*!
		\return The profile acceleration in [rad/s^2] at each device.
	*/
	std::vector<double> getProfileAcceleration();

	//! Profile acceleration mutator.
	/*!
		\param __profile_acceleration The profile acceleration in [rad/s^2].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setProfileAcceleration(const double & __profile_acceleration, const unsigned int & __device_number);

	//! Profile acceleration mutator.
	/*!
		\param __profile_accelerations The profile acceleration in [rad/s^2] for each device.
		\return false if size of \p __profile_accelerations does not match the number of devices, true otherwise.
	*/
	bool setProfileAcceleration(const std::vector<double> & __profile_accelerations);

	//! Profile acceleration mutator.
	/*! Sets the same value at all devices.
		\param __profile_acceleration The profile acceleration in [rad/s^2] for each device.
	*/
	void setProfileAcceleration(const double & __profile_acceleration);

	//! Profile deceleration accessor.
	/*!
		\param __device_number Device number.
		\return The profile deceleration in [rad/s^2].
	*/
	double getProfileDeceleration(const unsigned int & __device_number);

	//! Profile deceleration accessor.
	/*!
		\return The profile deceleration in [rad/s^2] at each device.
	*/
	std::vector<double> getProfileDeceleration();

	//! Profile deceleration mutator.
	/*!
		\param __profile_deceleration The profile deceleration in [rad/s^2].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setProfileDeceleration(const double & __profile_deceleration, const unsigned int & __device_number);

	//! Profile deceleration mutator.
	/*!
		\param __profile_decelerations The profile deceleration in [rad/s^2] for each device.
		\return false if size of \p __profile_decelerations does not match the number of devices, true otherwise.
	*/
	bool setProfileDeceleration(const std::vector<double> & __profile_decelerations);

	//! Profile deceleration mutator.
	/*! Sets the same value at all devices.
		\param __profile_deceleration The profile deceleration in [rad/s^2].
	*/
	void setProfileDeceleration(const double & __profile_deceleration);

	//! Profile velocity accessor.
	/*!
		\param __device_number Device number.
		\return The profile velocity in [rad/s].
	*/
	double getProfileVelocity(const unsigned int & __device_number);

	//! Profile velocity accessor.
	/*!
		\return The profile velocity in [rad/s] at each device.
	*/
	std::vector<double> getProfileVelocity();

	//! Profile velocity mutator.
	/*!
		\param __profile_velocity The profile velocity in [rad/s].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setProfileVelocity(const double & __profile_velocity, const unsigned int & __device_number);

	//! Profile velocity mutator.
	/*!
		\param __profile_velocities The profile velocity in [rad/s] for each device.
		\return false if size of \p __profile_velocities does not match the number of devices, true otherwise.
	*/
	bool setProfileVelocity(const std::vector<double> & __profile_velocities);

	//! Profile velocity mutator.
	/*! Sets the same value for all devices.
		\param __profile_velocity The profile velocity in [rad/s] for each device.
	*/
	void setProfileVelocity(const double & __profile_velocity);

	//! Max profile velocity accessor.
	/*!
		\param __device_number Device number.
		\return The max profile velocity in [rad/s].
	*/
	double getMaxProfileVelocity(const unsigned int & __device_number);

	//! Max profile velocity accessor.
	/*!
		\return The max profile velocity in [rad/s] at each device.
	*/
	std::vector<double> getMaxProfileVelocity();

	//! Max profile velocity mutator.
	/*!
		\param __max_velocity The max profile velocity in [rad/s].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setMaxProfileVelocity(const double & __max_velocity, const unsigned int & __device_number);

	//! Max profile velocity mutator.
	/*!
		\param __max_velocities The max profile velocity in [rad/s] for each device.
		\return false if size of \p __max_velocities does not match the number of devices, true otherwise.
	*/
	bool setMaxProfileVelocity(const std::vector<double> & __max_velocities);

	//! Max profile velocity mutator.
	/*! Sets the same value for all devices.
		\param __max_velocity The max profile velocity in [rad/s] for each device.
	*/
	void setMaxProfileVelocity(const double & __max_velocity);

	//! Max acceleration accessor.
	/*!
		\param __device_number Device number.
		\return The max acceleration in [rad/s^2].
	*/
	double getMaxAcceleration(const unsigned int & __device_number);

	//! Max acceleration accessor.
	/*!
		\return The max acceleration in [rad/s^2] at each device.
	*/
	std::vector<double> getMaxAcceleration();

	//! Max acceleration mutator.
	/*!
		\param __max_acceleration The max profile acceleration in [rad/s^2].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setMaxAcceleration(const double & __max_acceleration, const unsigned int & __device_number);

	//! Max acceleration mutator.
	/*!
		\param __max_accelerations The max profile acceleration in [rad/s] for each device.
		\return false if size of \p __max_accelerations does not match the number of devices, true otherwise.
	*/
	bool setMaxAcceleration(const std::vector<double> & __max_accelerations);

	//! Max acceleration mutator.
	/*! Sets the same value for all devices.
		\param __max_acceleration The max profile acceleration in [rad/s] for each device.
	*/
	void setMaxAcceleration(const double & __max_acceleration);

	//! Max deceleration accessor.
	/*!
		\param __device_number Device number.
		\return The max deceleration in [rad/s^2].
	*/
	double getMaxDeceleration(const unsigned int & __device_number);

	//! Max deceleration accessor.
	/*!
		\return The max deceleration in [rad/s^2] at each device.
	*/
	std::vector<double> getMaxDeceleration();

	//! Max deceleration mutator.
	/*!
		\param __max_deceleration The max profile deceleration in [rad/s^2].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setMaxDeceleration(const double & __max_deceleration, const unsigned int & __device_number);

	//! Max deceleration mutator.
	/*!
		\param __max_decelerations The max profile deceleration in [rad/s] for each device.
		\return false if size of \p __max_decelerations does not match the number of devices, true otherwise.
	*/
	bool setMaxDeceleration(const std::vector<double> & __max_decelerations);

	//! Max deceleration mutator.
	/*! Sets the same value for all devices.
		\param __max_deceleration The max profile deceleration in [rad/s] for each device.
	*/
	void setMaxDeceleration(const double & __max_deceleration);

	//! Device name accessor.
	/*!
		\param __device_number Device number.
		\return The device name, as reported by the soem library.
	*/
	std::string getDeviceName(const unsigned int & __device_number);

	//! Device name accessor.
	/*!
		\return The device name, as reported by the soem library, for each device.
	*/
	std::vector<std::string> getDeviceName();

	//! Drive name accessor.
	/*!
		\param __device_number Device number.
		\return The drive name, read from the device registry.
	*/
	std::string getDriveName(const unsigned int & __device_number);

	//! Drive name accessor.
	/*!
		\return The drive name, read from the device registry, at each device.
	*/
	std::vector<std::string> getDriveName();

	//! Drive name mutator.
	/*!
		\param __drive_name The desired drive name (maximum 8 characters long).
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct or __drive_name longer than 8 characters, true otherwise.
	*/
	bool setDriveName(const std::string & __drive_name, const unsigned int & __device_number);

	//! Drive name mutator.
	/*!
		\param __drive_names The desired drive name (maximum 8 characters long) for each device.
		\return false if size of \p __drive_names does not match the number of devices or some name longer than 8 characters, true otherwise.
	*/
	bool setDriveName(const std::vector<std::string> & __drive_names);

	//! Drive name mutator.
	/*! Sets the same name to all devices.
		\param __drive_name The desired drive name (maximum 8 characters long) for each device.
	*/
	bool setDriveName(const std::string & __drive_name);

	//! Position control proportional constant accessor.
	/*!
		\param __device_number Device number.
		\return The position control proportional constant in [Nm/rad].
	*/
	double getPositionControlProportionalConstant(const unsigned int & __device_number);

	//! Position control proportional constant accessor.
	/*!
		\return The position control proportional constant in [Nm/rad] at each device.
	*/
	std::vector<double> getPositionControlProportionalConstant();

	//! Position control proportional constant mutator.
	/*!
		\param __proportional_constant The desired proportional constant in [Nm/rad].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setPositionControlProportionalConstant(const double & __proportional_constant, const unsigned int & __device_number);

	//! Position control proportional constant mutator.
	/*!
		\param __proportional_constants The desired proportional constant in [Nm/rad] for each device.
		\return false if size of \p __proportional_constants does not match the number of devices, true otherwise.
	*/
	bool setPositionControlProportionalConstant(const std::vector<double> & __proportional_constants);

	//! Position control proportional constant mutator.
	/*! Sets the same value for all devices.
		\param __proportional_constant The desired proportional constant in [Nm/rad] for each device.
	*/
	void setPositionControlProportionalConstant(double & __proportional_constant);

	//! Position control integral constant accessor.
	/*!
		\param __device_number Device number.
		\return The position control integral constant in [Nm/(rad*s)].
	*/
	double getPositionControlIntegralConstant(const unsigned int & __device_number);

	//! Position control integral constant accessor.
	/*!
		\return The position control integral constant in [Nm/(rad*s)] at each device.
	*/
	std::vector<double> getPositionControlIntegralConstant();

	//! Position control integral constant mutator.
	/*!
		\param __integral_constant The desired integral constant in [Nm/(rad*s)].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setPositionControlIntegralConstant(const double & __integral_constant, const unsigned int & __device_number);

	//! Position control integral constant mutator.
	/*!
		\param __integral_constants The desired integral constant in [Nm/(rad*s)] for each device.
		\return false if size of \p __integral_constants does not match the number of devices, true otherwise.
	*/
	bool setPositionControlIntegralConstant(const std::vector<double> & __integral_constants);

	//! Position control integral constant mutator.
	/*! Sets the same value for all devices.
		\param __integral_constant The desired integral constant in [Nm/(rad*s)] for all devices.
	*/
	void setPositionControlIntegralConstant(const double & __integral_constant);

	//! Position control derivative constant accessor.
	/*!
		\param __device_number Device number.
		\return The position control derivative constant in [Nm*s/rad].
	*/
	double getPositionControlDerivativeConstant(const unsigned int & __device_number);

	//! Position control derivative constant accessor.
	/*!
		\return The position control derivative constant in [Nm*s/rad] at each device.
	*/
	std::vector<double> getPositionControlDerivativeConstant();

	//! Position control derivative constant mutator.
	/*!
		\param __derivative_constant The desired derivative constant in [Nm*s/rad].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setPositionControlDerivativeConstant(const double & __derivative_constant, const unsigned int & __device_number);

	//! Position control derivative constant mutator.
	/*!
		\param __derivative_constants The desired derivative constant in [Nm*s/rad] for each device.
		\return false if size of \p __derivative_constants does not match the number of devices, true otherwise.
	*/
	bool setPositionControlDerivativeConstant(const std::vector<double> & __derivative_constants);

	//! Position control derivative constant mutator.
	/*! Sets the same value for all devices.
		\param __derivative_constant The desired derivative constant in [Nm*s/rad] for all devices.
	*/
	void setPositionControlDerivativeConstant(const double & __derivative_constant);

	//! Velocity control proportional constant accessor.
	/*!
		\param __device_number Device number.
		\return The velocity control proportional constant in [Nm/(rad*s)].
	*/
	double getVelocityControlProportionalConstant(const unsigned int & __device_number);

	//! Velocity control proportional constant accessor.
	/*!
		\return The velocity control proportional constant in [Nm/(rad*s)] at each device.
	*/
	std::vector<double> getVelocityControlProportionalConstant();

	//! Velocity control proportional constant mutator.
	/*!
		\param __proportional_constant The desired proportional constant in [Nm/rad].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setVelocityControlProportionalConstant(const double & __proportional_constant, const unsigned int & __device_number);

	//! Velocity control proportional constant mutator.
	/*!
		\param __proportional_constants The desired proportional constant in [Nm/rad] for each device.
		\return false if size of \p __proportional_constants does not match the number of devices, true otherwise.
	*/
	bool setVelocityControlProportionalConstant(const std::vector<double> & __proportional_constants);

	//! Velocity control proportional constant mutator.
	/*! Sets the same value for all devices.
		\param __proportional_constant The desired proportional constant in [Nm/rad] for each device.
	*/
	void setVelocityControlProportionalConstant(double & __proportional_constant);

	//! Velocity control integral constant accessor.
	/*!
		\param __device_number Device number.
		\return The velocity control integral constant in [Nm/(rad*s^2)].
	*/
	double getVelocityControlIntegralConstant(const unsigned int & __device_number);

	//! Velocity control integral constant accessor.
	/*!
		\return The velocity control integral constant in [Nm/(rad*s^2)] at each device.
	*/
	std::vector<double> getVelocityControlIntegralConstant();

	//! Velocity control integral constant mutator.
	/*!
		\param __integral_constant The desired integral constant in [Nm/(rad*s)].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setVelocityControlIntegralConstant(const double & __integral_constant, const unsigned int & __device_number);

	//! Velocity control integral constant mutator.
	/*!
		\param __integral_constants The desired integral constant in [Nm/(rad*s)] for each device.
		\return false if size of \p __integral_constants does not match the number of devices, true otherwise.
	*/
	bool setVelocityControlIntegralConstant(const std::vector<double> & __integral_constants);

	//! Velocity control integral constant mutator.
	/*! Sets the same value for all devices.
		\param __integral_constant The desired integral constant in [Nm/(rad*s)] for all devices.
	*/
	void setVelocityControlIntegralConstant(const double & __integral_constant);

	//! Velocity control derivative constant accessor.
	/*!
		\param __device_number Device number.
		\return The velocity control derivative constant in [Nm/rad].
	*/
	double getVelocityControlDerivativeConstant(const unsigned int & __device_number);

	//! Velocity control derivative constant accessor.
	/*!
		\return The velocity control derivative constant in [Nm/rad] at each device.
	*/
	std::vector<double> getVelocityControlDerivativeConstant();

	//! Velocity control derivative constant mutator.
	/*!
		\param __derivative_constant The desired derivative constant in [Nm*s/rad].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setVelocityControlDerivativeConstant(const double & __derivative_constant, const unsigned int & __device_number);

	//! Velocity control derivative constant mutator.
	/*!
		\param __derivative_constants The desired derivative constant in [Nm*s/rad] for each device.
		\return false if size of \p __derivative_constants does not match the number of devices, true otherwise.
	*/
	bool setVelocityControlDerivativeConstant(const std::vector<double> & __derivative_constants);

	//! Velocity control derivative constant mutator.
	/*! Sets the same value for all devices.
		\param __derivative_constant The desired derivative constant in [Nm*s/rad] for all devices.
	*/
	void setVelocityControlDerivativeConstant(const double & __derivative_constant);

	//! Max current accessor.
	/*!
		\param __device_number Device number.
		\return The max current in [A].
	*/
	double getMaxCurrent(const unsigned int & __device_number);

	//! Max current accessor.
	/*!
		\return The max current in [A] at each device.
	*/
	std::vector<double> getMaxCurrent();

	//! Max current mutator.
	/*!
		\param __max_current The max current in [A].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setMaxCurrent(const double & __max_current, const unsigned int & __device_number);

	//! Max current mutator.
	/*!
		\param __max_currents The max current in [A] for each device.
		\return false if size of \p __max_currents does not match the number of devices, true otherwise.
	*/
	bool setMaxCurrent(const std::vector<double> & __max_currents);

	//! Max current mutator.
	/*! Sets the same value for all devices.
		\param __max_current The max current in [A] for all devices.
	*/
	void setMaxCurrent(const double & __max_current);

	//! Max motor speed accessor.
	/*!
		\param __device_number Device number.
		\return The max motor speed in [rad/s].
	*/
	double getMaxMotorSpeed(const unsigned int & __device_number);

	//! Max motor speed accessor.
	/*!
		\return The max motor speed in [rad/s] at each device.
	*/
	std::vector<double> getMaxMotorSpeed();

	//! Max motor speed mutator.
	/*!
		\param __max_motor_speed The max motor speed in [rad/s].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setMaxMotorSpeed(const double & __max_motor_speed, const unsigned int & __device_number);

	//! Max motor speed mutator.
	/*!
		\param __max_motor_speeds The max motor speed in [rad/s] for each device.
		\return false if size of \p __max_motor_speeds does not match the number of devices, true otherwise.
	*/
	bool setMaxMotorSpeed(const std::vector<double> & __max_motor_speeds);

	//! Max motor speed mutator.
	/*! Sets the same value for all devices.
		\param __max_motor_speed The max motor speed in [rad/s] for all devices.
	*/
	void setMaxMotorSpeed(const double & __max_motor_speed);

	//! Error register accessor.
	/*!
		\param __device_number Device number.
		\return The error register.
	*/
	uint8 getErrorRegister(const unsigned int & __device_number);

	//! Error register accessor.
	/*!
		\return The error register at each device.
	*/
	std::vector<uint8> getErrorRegister();

	//! History number of errors accessor.
	/*!
		\param __device_number Device number.
		\return The number of errors stored in the pre-defined error field.
	*/
	uint8 getHistoryNumberOfErrors(const unsigned int & __device_number);

	//! History number of errors accessor.
	/*!
		\return The number of errors stored in the pre-defined error field at each device.
	*/
	std::vector<uint8> getHistoryNumberOfErrors();

	//! History errors accessor.
	/*!
		\param __device_number Device number.
		\return The error codes stored in the pre-defined error field.
	*/
	std::vector<uint32> getHistoryErrors(const unsigned int & __device_number);

	//! History errors accessor.
	/*!
		\return The error codes stored in the pre-defined error field at each device.
	*/
	std::vector<std::vector<uint32>> getHistoryErrors();

	//! Last error code accessor.
	/*!
		\param __device_number Device number.
		\return The last known error code.
	*/
	uint16 getLastErrorCode(const unsigned int & __device_number);

	//! Last error code accessor.
	/*!
		\return The last known error code at each device.
	*/
	std::vector<uint16> getLastErrorCode();

	//! Last error code in readable form accessor.
	/*!
		\param __device_number Device number.
		\return A string representing the last known error code.
	*/
	std::string getLastErrorAsString(const unsigned int & __device_number);

	//! Last error code in readable form accessor.
	/*!
		\return A string representing the last known error code at each device.
	*/
	std::vector<std::string> getLastErrorAsString();

	//! Vendor ID accessor.
	/*!
		\param __device_number Device number.
		\return The vendor ID.
	*/
	uint32 getIdentityVendorID(const unsigned int & __device_number);

	//! Vendor ID accessor.
	/*!
		\return The vendor ID at each device.
	*/
	std::vector<uint32> getIdentityVendorID();

	//! Product code accessor.
	/*!
		\param __device_number Device number.
		\return The product code.
	*/
	uint32 getIdentityProductCode(const unsigned int & __device_number);

	//! Product code accessor.
	/*!
		\return The product code at each device.
	*/
	std::vector<uint32> getIdentityProductCode();

	//! Revision number accessor.
	/*!
		\param __device_number Device number.
		\return The revision number.
	*/
	uint32 getIdentityRevisionNumber(const unsigned int & __device_number);

	//! Revision number accessor.
	/*!
		\return The revision number at each device.
	*/
	std::vector<uint32> getIdentityRevisionNumber();

	//! Serial number accessor.
	/*!
		\param __device_number Device number.
		\return The serial number.
	*/
	uint32 getIdentitySerialNumber(const unsigned int & __device_number);

	//! Serial number accessor.
	/*!
		\return The serial number at each device.
	*/
	std::vector<uint32> getIdentitySerialNumber();

	//! Motor type accessor.
	/*!
		\param __device_number Device number.
		\return The motor type.
	*/
	uint16 getMotorType(const unsigned int & __device_number);

	//! Motor type accessor.
	/*!
		\return The motor type at each device.
	*/
	std::vector<uint16> getMotorType();

	//! Quick stop option code accessor.
	/*!
		\param __device_number Device number.
		\return The quick stop option code.
	*/
	int16 getQuickStopOptionCode(const unsigned int & __device_number);

	//! Quick stop option code accessor.
	/*!
		\return The quick stop option code at each device.
	*/
	std::vector<int16> getQuickStopOptionCode();

	//! Quick stop option code mutator.
	/*!
		\param __quick_stop_option_code The quick stop option code.
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setQuickStopOptionCode(const int16 & __quick_stop_option_code, const unsigned int & __device_number);

	//! Quick stop option code mutator.
	/*!
		\param __quick_stop_option_codes The quick stop option code for each device.
		\return false if size of \p __quick_stop_option_codes does not match the number of devices, true otherwise.
	*/
	bool setQuickStopOptionCode(const std::vector<int16> & __quick_stop_option_codes);

	//! Quick stop option code mutator.
	/*! Sets the same value for all devices.
		\param __quick_stop_option_code The quick stop option code for all devices.
	*/
	void setQuickStopOptionCode(const int16 & __quick_stop_option_code);

	//! Position demand value accessor.
	/*!
		The position demand is the output generated by the drive's profiler and it is used as position control input.
		\param __device_number Device number.
		\return The position demand internal value in [rad].
	*/
	double getPositionDemandValue(const unsigned int & __device_number);

	//! Position demand value accessor.
	/*!
		The position demand is the output generated by the drive's profiler and it is used as position control input.
		\return The position demand internal value in [rad] at each device.
	*/
	std::vector<double> getPositionDemandValue();

	//! Positioning relative option accessor.
	/*!
		\param __device_number Device number.
		\return The positioning relative option (see MultiDevice::setPositioningRelativeOption for a description of possible options).
	*/
	uint16 getPositioningRelativeOption(const unsigned int & __device_number);

	//! Positioning relative option accessor.
	/*!
		\return The positioning relative option at each device (see MultiDevice::setPositioningRelativeOption for a description of possible options).
	*/
	std::vector<uint16> getPositioningRelativeOption();

	//! Positioning relative option mutator.
	/*! Can take the following values:
		- 0 -> Positioning moves are performed relative to the preceding (internal absolute) target position (rsp. relative to 0 if there is no preceding target position).
		- 1 -> Positioning moves are performed relative to the actual position demand value (see MultiDevice::getPositionDemandValue).
		- 2 -> Positioning moves are performed relative to the position actual value (see MultiDevice::getActualPosition).
		\param __option The desired option.
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number or \p __option is not correct, true otherwise.
	*/
	bool setPositioningRelativeOption(const uint16 & __option, const unsigned int & __device_number);

	//! Positioning relative option mutator.
	/*!
		\param __options The desired option for each device.
		\return false if size of \p __options does not match the number of devices or some value in \p __options is not correct, true otherwise.
	*/
	bool setPositioningRelativeOption(const std::vector<uint16> & __options);

	//! Positioning relative option mutator.
	/*! Sets the same value for all devices.
		\param __option The desired option for all devices.
		\return false if \p __option is not correct, true otherwise.
	*/
	bool setPositioningRelativeOption(const uint16 & __option);

	//! Positioning rotary axis option accessor
	/*!
		\param __device_number Device number
		\return The positioning rotary axis option (see MultiDevice::setPositioningRotaryAxisOption for a description of possible options)
	*/
	uint16 getPositioningRotaryAxisOption(const unsigned int & __device_number);

	//! Positioning rotary axis option accessor
	/*!
		\return The positioning rotary axis option at each device (see MultiDevice::setPositioningRotaryAxisOption for a description of possible options)
	*/
	std::vector<uint16> getPositioningRotaryAxisOption();

	//! Positioning rotary axis option mutator.
	/*! The rotary axis option controls the behavior of the Modulo functionality. Can take the following values:
		- 0 -> Normal positioning similar to linear axis. If reaching or exceeding the position range limit (see MultiDevice::getPositionRangeLimit), the input value wraps automatically to the other end of the range. Positioning can be relative or absolute (see MultiDevice::setTargetPositionRelative). Only with this option, the movement greater than a modulo value is possible.
		- 64 -> Positioning only in negative direction.
		- 128 -> Positioning only in positive direction.
		- 192 -> Positioning with the shortest way to the target position (positive direction if same in both directions).
		\param __option The desired option.
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number or \p __option is not correct, true otherwise.
	*/
	bool setPositioningRotaryAxisOption(const uint16 & __option, const unsigned int & __device_number);

	//! Positioning rotary axis option mutator.
	/*!
		\param __options The desired option for each device.
		\return false if size of \p __options does not match the number of devices or some value in \p __options is not correct, true otherwise.
	*/
	bool setPositioningRotaryAxisOption(const std::vector<uint16> & __options);

	//! Positioning rotary axis option mutator.
	/*! Sets the same value for all devices.
		\return false if \p __option is not correct, true otherwise.
	*/
	bool setPositioningRotaryAxisOption(const uint16 & __option);

	//! Max position range limit accessor.
	/*!
		\param __device_number Device number.
		\return Max position range limit in [rad].
	*/
	double getMaxPositionRangeLimit(const unsigned int & __device_number);

	//! Max position range limit accessor.
	/*!
		\return Max position range limit at each device in [rad]
	*/
	std::vector<double> getMaxPositionRangeLimit();

	//! Max position range limits mutator.
	/*!
		\param __max_limit Max range limit in [rad].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setMaxPositionRangeLimit(const double & __max_limit, const unsigned int & __device_number);

	//! Max position range limits mutator.
	/*!
		\param __max_limits Max range limit in [rad] for each device.
		\return false if size of \p __max_limits does not match the number of devices, true otherwise.
	*/
	bool setMaxPositionRangeLimit(const std::vector<double> & __max_limits);

	//! Max position range limits mutator.
	/*! Sets the same value for all devices.
		\param __max_limit Max range limit in [rad] for all devices.
	*/
	void setMaxPositionRangeLimit(const double & __max_limit);

	//! Min position range limit accessor.
	/*!
		\param __device_number Device number.
		\return Min position range limit in [rad].
	*/
	double getMinPositionRangeLimit(const unsigned int & __device_number);

	//! Min position range limit accessor.
	/*!
		\return Min position range limit at each device in [rad].
	*/
	std::vector<double> getMinPositionRangeLimit();

	//! Min position range limits mutator.
	/*!
		\param __min_limit Min range limit in [rad].
		\param __device_number Device number (starting from 0).
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setMinPositionRangeLimit(const double & __min_limit, const unsigned int & __device_number);

	//! Min position range limits mutator.
	/*!
		\param __min_limits Min range limit in [rad] for each device.
		\return false if size of \p __min_limits does not match the number of devices, true otherwise.
	*/
	bool setMinPositionRangeLimit(const std::vector<double> & __min_limits);

	//! Min position range limits mutator.
	/*! Sets the same value for all devices.
		\param __min_limit Min range limit in [rad] for all devices.
	*/
	void setMinPositionRangeLimit(const double & __min_limit);

	//! Manufacturer device name accessor.
	/*!
		\param __device_number Device number.
		\return The manufacturer device name.
	*/
	std::string getManufacturerDeviceName(const unsigned int & __device_number);

	//! Manufacturer device name accessor.
	/*!
		\return The manufacturer device name at each device.
	*/
	std::vector<std::string> getManufacturerDeviceName();

	//! Manufacturer hardware name accessor.
	/*!
		\param __device_number Device number.
		\return The manufacturer hardware name.
	*/
	std::string getManufacturerHardwareName(const unsigned int & __device_number);

	//! Manufacturer hardware name accessor.
	/*!
		\return The manufacturer hardware name at each device.
	*/
	std::vector<std::string> getManufacturerHardwareName();

	//! Software version accessor.
	/*!
		\param __device_number Device number.
		\return The software version.
	*/
	std::string getSoftwareVersion(const unsigned int & __device_number);

	//! Software version accessor.
	/*!
		\return The software version at each device.
	*/
	std::vector<std::string> getSoftwareVersion();

	//! HTTP drive catalog address.
	/*!
		\param __device_number Device number.
		\return The HTTP drive catalog address.
	*/
	std::string getHTTPDriveCatalogAddress(const unsigned int & __device_number);

	//! HTTP drive catalog address.
	/*!
		\return The HTTP drive catalog address at each device.
	*/
	std::vector<std::string> getHTTPDriveCatalogAddress();

	//! Max allowed torque mutator
	/*!
		\param __device_number Device number
		\param __max_allowed_torque The max allowed torque in per mil of rated torque (see MultiDevice::getRatedTorque)
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setMaxAllowedTorque(const int & __max_allowed_torque, const unsigned int & __device_number);

	//! Max allowed torque mutator.
	/*!
		\param __max_allowed_torques The max allowed torque in per mil of rated torque (see MultiDevice::getRatedTorque) for each device
		\return false if size of \p __max_allowed_torques does not match the number of devices, true otherwise.
	*/
	bool setMaxAllowedTorque(const std::vector<int> & __max_allowed_torques);

	//! Max allowed torque mutator.
	/*! Sets the same value for all devices.
		\param __max_allowed_torque The max allowed torque in per mil of rated torque (see MultiDevice::getRatedTorque) for all device
	*/
	void setMaxAllowedTorque(const int & __max_allowed_torque);

	//! Sets the desired target position at each device.
	/*!
		\param __target_position Desired positions in [rad].
	*/
	void setTargetPosition(const std::vector<double> & __target_position);

	//! Sets the desired target velocities at each device.
	/*!
		\param __target_velocity Desired velocities in [rad/s].
	*/
	void setTargetVelocity(const std::vector<double> & __target_velocity);

	//! Sets the desired target torques at each device.
	/*!
		\param __target_torque Desired torques in [rad/s].
	*/
	void setTargetTorque(const std::vector<double> & __target_torque);

	//! Sets the target position relative option for a device (false by default).
	/*! The relative positioning behaviour depends on the positioning relative option (see MultiDevice::setPositioningRelativeOption).
		\param __relative if true, target positions are relative to last target position; if false, target positions are absolute.
		\param __device_number Device number.
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool setTargetPositionRelative(const bool __relative, const unsigned int & __device_number);

	//! Sets the target position relative option at each device (false by default).
	/*! The relative positioning behaviour depends on the positioning relative option (see MultiDevice::setPositioningRelativeOption).
		\param __relatives if true, target positions are relative to last target position; if false, target positions are absolute.
		\return false if size of \p __relatives does not match the number of devices, true otherwise.
	*/
	bool setTargetPositionRelative(const std::vector<bool> & __relatives);

	//! Sets the target position relative option for all devices (false by default).
	/*! The relative positioning behaviour depends on the positioning relative option (see MultiDevice::setPositioningRelativeOption).
		\param __relative if true, target positions are relative to last target position; if false, target positions are absolute.
	*/
	void setTargetPositionRelative(const bool __relative);

	//! Sets the position change set immediately option for all devices (false by default).
	/*!
		\param __change_set_immediately if true, next positioning shall be started immediately interrupting the actual one; if false, actual positioning will be completed before the next one gets started.
	*/
	void setPositionChangeSetImmediately(const bool __change_set_immediately);

	//! Triggers the execution of the current position command in relative positioning mode
	/*!
		\param __device_number Device number.
		\return false if \p __device_number is not correct, true otherwise.
	*/
	bool latchRelativePositionTarget(const unsigned int & __device_number);

	//! Triggers the execution of the current position command in relative positioning mode for all devices
	void latchRelativePositionTarget();

	//! Prints a string representing the last known error code for each device through the standard output
	void printLastError();

private:

	// utils for init
	bool initEthercat(const std::string & __if_name, const unsigned int & __num_devices);
	bool configureNetworkMemoryMap();
	bool enableEthercatOperationalState();
	void allocateData(const unsigned int & __num_devices);
	void readParamsFromDevices();
	bool resetFaultDevices(const double & __timeout_seconds);

	// state changes
	bool sendAndWaitForOperationModes(const std::vector<OperationMode> & __operation_mode, const double & __timeout_seconds);
	bool sendAndWaitForStates(const std::vector<StateMachineState> & __expected_state, const double & __timeout_seconds);
	void commandSequenceToEnable();
	void updateMultiDeviceState();
	bool allDevicesInState(const StateMachineState & __state_machine_state);
	bool someDeviceInFault();

	// deal with ethercat frame
	void codeFrame();
	void decodeFrame();

	// translations
	StateMachineState statusWord2StateMachineState(const uint16 & __status_word);
	uint16 stateMachineCommand2ControlWordCommand(const StateMachineCommand & __state_machine_command);
	std::string errorCode2String(uint16 & __error_code);
	std::string StateMachineState2String(const StateMachineState & __state_machine_state);

	// ethercat state handling
	void ethercatCheck();

	// other utils
	bool checkDeviceNumber(const unsigned int & __device_number);
	bool checkInputSize(const int & __size);
	bool checkDriveName(const std::string & __drive_name);
	bool checkPositioningRelativeOption(const uint16 & __option);
	bool checkPositioningRotaryAxisOption(const uint16 & __option);

	// utils for SDOs
	template<class T>
	void writeSDO(const uint16 __device_number, const uint16 __index, const uint8 __sub_index, const T __value)
	{
		T data = __value;
		int size_of_data = sizeof(data);
		ec_SDOwrite(__device_number+1, __index, __sub_index, FALSE, size_of_data, &data, EC_TIMEOUTRXM);
	};

	template<class T>
	void readSDO(const uint16 __device_number, const uint16 __index, const uint8 __sub_index, T * __buf)
	{
		T data = *__buf;
		int size_of_data = sizeof(data);
		ec_SDOread(__device_number+1, __index, __sub_index, FALSE, &size_of_data, &data, EC_TIMEOUTRXM);
		*__buf = data;
	};

	template<class T>
	void setValueAtDevice(const double & __value, const unsigned int & __device_number, const uint16 & __index, const uint8 & __sub_index)
	{
		T value = (T)( __value );
		int size_of_value = sizeof(value);
		ec_SDOwrite(__device_number+1, __index, __sub_index, FALSE, size_of_value, &value, EC_TIMEOUTRXM);
	}

	template<class T, class U>
	T getValueFromDevice(const unsigned int & __device_number, const uint16 & __index, const uint8 & __sub_index, const T & __device_to_SI_units)
	{
		U device_value;
		int size_of_device_value = sizeof(device_value);
		ec_SDOread(__device_number+1, __index, __sub_index, FALSE, &size_of_device_value, &device_value, EC_TIMEOUTRXM);
		return (T)(device_value) * __device_to_SI_units;
	}

	void setStringAtDevice(const std::string & __string, const unsigned int & __device_number, const uint16 & __index, const uint8 & __sub_index)
	{
		char char_array[__string.length() + 1];
		std::strcpy(char_array, __string.c_str());
		uint64 data64;
		std::memcpy(&data64, char_array, sizeof(uint64));
		int size_of_data = sizeof(data64);
		ec_SDOwrite(__device_number+1, __index, __sub_index, FALSE, size_of_data, &data64, EC_TIMEOUTRXM);
	}

	std::string getStringFromDevice(const unsigned int & __device_number, const uint16 & __index, const uint8 & __sub_index)
	{
		char string_data[128];
		int size_of_string_data = sizeof(string_data) - 1; // to make sure we will always have a 0 at the end
		std::memset(&string_data, 0, 128);
		ec_SDOread(__device_number+1, __index, __sub_index, FALSE, &size_of_string_data, &string_data, EC_TIMEOUTRXM);
		return std::string(string_data);
	}

};

}

#endif
