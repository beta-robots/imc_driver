#ifndef ___IMC_HWIFACE___IMC_HWIFACE_H___
#define ___IMC_HWIFACE___IMC_HWIFACE_H___

#include <ros/ros.h>
#include <urdf/model.h>

#include <hardware_interface/robot_hw.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <controller_manager/controller_manager.h>
#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_interface.h>
#include <joint_limits_interface/joint_limits_rosparam.h>
#include <joint_limits_interface/joint_limits_urdf.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_srvs/Trigger.h>

#include <imc_driver/multi_device.h>
#include <imc_driver/IMCDriver.h>
#include <imc_driver/SetIMCDriverParameters.h>

namespace imc_driver
{

/*! \class IMCHWIface
	\brief Hardware interface for imc_drives.

	This class implements a Robot Hardware Interface for imc_drives using the imc_driver::MultiDevice class.
*/
class IMCHWIface : public hardware_interface::RobotHW
{

protected:

	//! The imc_driver::MultiDevice instance
	imc_driver::MultiDevice imc_driver__;

private:

	ros::NodeHandle nh__;

	std::string eth_if_name__;
	std::vector<std::string> joint_names__;
	std::size_t num_joints__;

	// read-only interfaces
	hardware_interface::JointStateInterface joint_state_iface__;

	// read-write interface
	hardware_interface::PositionJointInterface position_joint_iface__;
	hardware_interface::VelocityJointInterface velocity_joint_iface__;
	hardware_interface::EffortJointInterface effort_joint_iface__;

	// state feedback
	ros::Publisher imc_driver_state_publisher__;
	imc_driver::IMCDriver imc_driver_state__;

	// quick stop, rearm and latch services
	ros::ServiceServer quick_stop_server__;
	ros::ServiceServer rearm_server__;
	ros::ServiceServer latch_relative_position_server__;

	// params server
	ros::ServiceServer parameters_server__;

	// software safety layer
	bool reset_controllers__;
	double software_safety_velocity_limit__;

public:

	//! Constructor
	/*! Initializes the ros node handle. */
	IMCHWIface()
	{
		nh__ = ros::NodeHandle();
	}

	//! Destructor
	/*! Deactivates and closes the imc_driver::MultiDevice. */
	~IMCHWIface()
	{
		imc_driver__.deactivate(10.0);
		imc_driver__.close();
	}

	//! The init method parses the joint names and ethernet interface name parameters,
	//! inits the imc_driver::MultiDevice instance,
	//! registers the joint state, position, velocity, and torque interfaces,
	//! and attempts the start sequence for the imc_driver::MultiDevice instance.
	/*!
		\return false if some parameter can not be read, the imc_driver::MultiDevice can not be initialized, or
		if the start sequence fails. True otherwise.
	*/
	bool init()
	{
		// set verbosity for imc_driver library
		bool driver_verbose;
		if( !nh__.getParam("driver_verbose", driver_verbose) )
		{
			ROS_ERROR("Failed to read parameter driver_verbose. Setting to false.");
			driver_verbose = false;
		}
		imc_driver__.setVerbose(driver_verbose);

		// parse URDF for number of joints with convention (optional TODO)
		if( !nh__.getParam("joint_names", joint_names__) )
		{
			ROS_ERROR("Failed to read parameter joint_names. This parameter is required to resize to the number of motors");
			return false;
		}
		num_joints__ = joint_names__.size();

		// get ethernet interface
		if( !nh__.getParam("eth_if_name", eth_if_name__) )
		{
			ROS_ERROR("Failed to read parameter eth_if_name. This parameter is required to start the ethercat master");
			return false;
		}

		// get default device parameters from parameter server
		imc_driver::SetIMCDriverParameters imc_driver_parameters;
		if ( !getDeviceParameters(imc_driver_parameters) )
			return false;

		// get max velocity for software safety layer
		if ( !nh__.getParam("software_safety_velocity_limit", software_safety_velocity_limit__) )
		{
			ROS_ERROR("Failed to read parameter software_safety_velocity_limit.");
			return false;
		}

		// init imc_driver
		if( !imc_driver__.init(eth_if_name__, num_joints__, 10.0) )
		{
			ROS_ERROR("IMC Driver can't initialize, check log error");
			return false;
		}

		// update device parameters with default values
		if ( !updateIMCDriverParameters(imc_driver_parameters.request.devices, imc_driver_parameters.response.devices) )
		{
			ROS_ERROR("Failed to set some default parameter");
			return false;
		}

		// register interfaces
		for(int j = 0; j < num_joints__; j++)
		{
			hardware_interface::JointStateHandle js_handle(joint_names__[j], &(imc_driver__.position_feedback__[j]), &(imc_driver__.velocity_feedback__[j]), &(imc_driver__.torque_feedback__[j]));
			joint_state_iface__.registerHandle(js_handle);

			hardware_interface::JointHandle jp_handle(joint_state_iface__.getHandle(joint_names__[j]), &(imc_driver__.position_command__[j]));
			position_joint_iface__.registerHandle(jp_handle);

			hardware_interface::JointHandle jv_handle(joint_state_iface__.getHandle(joint_names__[j]), &(imc_driver__.velocity_command__[j]));
			velocity_joint_iface__.registerHandle(jv_handle);

			hardware_interface::JointHandle je_handle(joint_state_iface__.getHandle(joint_names__[j]), &(imc_driver__.torque_command__[j]));
			effort_joint_iface__.registerHandle(je_handle);

			ROS_INFO_STREAM("Joint [" << "\x1B[37m" << joint_names__[j]  << "\x1B[0m" << "] registered in position, velocity and effort interface");
		}
		registerInterface(&joint_state_iface__);
		registerInterface(&position_joint_iface__);
		registerInterface(&velocity_joint_iface__);
		registerInterface(&effort_joint_iface__);

		// start publisher for state machine state feedback
		imc_driver_state__.devices.resize(num_joints__);
		imc_driver_state_publisher__ = nh__.advertise<imc_driver::IMCDriver>("imc_driver_state", 1, true);

		// start servers for quick stop and rearm
		reset_controllers__ = false;
		quick_stop_server__ = nh__.advertiseService("quick_stop_request", &IMCHWIface::quickStopServerCallback, this);
		rearm_server__ = nh__.advertiseService("rearm_request", &IMCHWIface::rearmServerCallback, this);
		latch_relative_position_server__ = nh__.advertiseService("latch_relative_position", &IMCHWIface::latchRelativePositionServerCallback, this);

		// start server to update parameters
		parameters_server__ = nh__.advertiseService("parameters_server", &IMCHWIface::parametersCallback, this);

		// start up multi device
		reset_controllers__ = true;
		imc_driver__.setOperationMode(imc_driver::OperationMode::PROFILE_VELOCITY);
		if ( !imc_driver__.activate(10.0) )
		{
			ROS_ERROR("Failed to activate IMC Driver, check log error");
			return false;
		}

		return true;
	}

	//! Reads from devices through the imc_driver::MultiDevice instance.
	/*!
		\param __delta_time
	*/
	void read(ros::Duration& __delta_time)
	{
		imc_driver__.read();

		// software safety layer
		for ( auto & device_velocity : imc_driver__.velocity_feedback__)
		{
			if ( std::abs(device_velocity) > software_safety_velocity_limit__ )
			{
				imc_driver__.quickStop();
				break;
			}
		}

		// reset controllers until operation is enabled again
		if ( imc_driver__.getMultiDeviceState() != imc_driver::MultiDeviceState::ENABLED )
			reset_controllers__ = true;

		updateIMCDriverStateAndPublish();
	}

	//! Writes to devices through the imc_driver::MultiDevice instance.
	/*!
		\param __delta_time
	*/
	void write(ros::Duration& __delta_time)
	{
		imc_driver__.write();
	}

	//! Prepares a controller switch.
	/*!
		\param __start_list list of controllers to start. This interface can accept only a single controller to start.
		\param __stop_list list of controllers to stop
		\return true if controller switch can be done, false otherwise.
	*/
	bool prepareSwitch(const std::list<hardware_interface::ControllerInfo>& __start_list, const std::list<hardware_interface::ControllerInfo>& __stop_list)
	{
		if( (__start_list.size() > 1) || (__stop_list.size() > 1) )
		{
			ROS_WARN("This interface can only accept a single controller");
			return false;
		}
		return true;
	}

	//! Performs a controller switch.
	/*!
		\param __start_list list of controllers to start
		\param __stop_list list of controllers to stop
	*/
	void doSwitch(const std::list<hardware_interface::ControllerInfo>& __start_list, const std::list<hardware_interface::ControllerInfo>& __stop_list)
	{
		// at this point, we know there is only one controller, and that we assume we are working with
		// single interface controllers, so we can safely do this:
		if( __start_list.front().claimed_resources.front().hardware_interface == "hardware_interface::PositionJointInterface" )
		{
			imc_driver__.setOperationMode(imc_driver::OperationMode::PROFILE_POSITION);
		}
		if( __start_list.front().claimed_resources.front().hardware_interface == "hardware_interface::VelocityJointInterface" )
		{
			imc_driver__.setOperationMode(imc_driver::OperationMode::PROFILE_VELOCITY);
		}
		if( __start_list.front().claimed_resources.front().hardware_interface == "hardware_interface::EffortJointInterface" )
		{
			imc_driver__.setOperationMode(imc_driver::OperationMode::PROFILE_TORQUE);
		}
		return;
	}

	bool resetControllersRequired()
	{
		if ( reset_controllers__ )
		{
			reset_controllers__ = false;
			return true;
		}
		return false;
	}

private:

	void updateIMCDriverStateAndPublish()
	{
		std::vector<StateMachineState> drive_states = imc_driver__.getStateMachineState();
		std::vector<std::string> drive_state_strings = imc_driver__.getStateMachineStateAsString();
		std::vector<uint16> ethercat_state = imc_driver__.getEthercatState();
		std::vector<std::string> error_strings = imc_driver__.getLastErrorAsString();
		for ( int i = 0; i < drive_states.size(); i++ )
		{
			imc_driver_state__.devices.at(i).drive_state = drive_states.at(i);
			imc_driver_state__.devices.at(i).drive_state_string = drive_state_strings.at(i);
			imc_driver_state__.devices.at(i).ethercat_state = ethercat_state.at(i);
			imc_driver_state__.devices.at(i).last_error_string = error_strings.at(i);
		}
		imc_driver_state__.state = imc_driver__.getMultiDeviceState();
		imc_driver_state_publisher__.publish( imc_driver_state__ );
	}

	bool quickStopServerCallback(std_srvs::Trigger::Request & __request, std_srvs::Trigger::Response & __response)
	{
		imc_driver__.quickStop();
		__response.message = "Quick stop requested";
		return true;
	}

	bool rearmServerCallback(std_srvs::Trigger::Request & __request, std_srvs::Trigger::Response & __response)
	{
		imc_driver__.enable();
		__response.message = "Rearm requested";
		return true;
	}

	bool latchRelativePositionServerCallback(std_srvs::Trigger::Request & __request, std_srvs::Trigger::Response & __response)
	{
		imc_driver__.latchRelativePositionTarget();
		__response.message = "Latch requested";
		return true;
	}

	bool parametersCallback(imc_driver::SetIMCDriverParameters::Request & __request, imc_driver::SetIMCDriverParameters::Response & __response)
	{
		return updateIMCDriverParameters(__request.devices, __response.devices);
	}

	bool updateIMCDriverParameters(const std::vector<imc_driver::DeviceParametersUpdateRequest> & __device_requests, std::vector<imc_driver::DeviceParametersUpdateResponse> & __device_responses)
	{
		for ( auto device_update : __device_requests )
		{
			imc_driver::DeviceParametersUpdateResponse device_response;
			device_response.device_number = device_update.device_number;
			if ( (device_update.device_number < 1) || (device_update.device_number > num_joints__) )
			{
				ROS_WARN_STREAM("Incorrect device number (" << device_update.device_number <<"), skipping this device parameters update request");
				device_response.update_success.push_back(false);
				__device_responses.push_back(device_response);
				break;
			}
			for ( auto parameter : device_update.parameters_to_update )
			{
				device_response.processed_parameters.push_back(parameter);
				bool success;
				switch ( parameter )
				{
					case imc_driver::DeviceParametersUpdateRequest::MAX_ALLOWED_TORQUE:
						success = imc_driver__.setMaxAllowedTorque(device_update.max_allowed_torque, device_update.device_number-1);
						break;
					case imc_driver::DeviceParametersUpdateRequest::PROFILE_ACCELERATION:
						success = imc_driver__.setProfileAcceleration(device_update.profile_acceleration, device_update.device_number-1);
						break;
					case imc_driver::DeviceParametersUpdateRequest::PROFILE_DECELERATION:
						success = imc_driver__.setProfileDeceleration(device_update.profile_deceleration, device_update.device_number-1);
						break;
					case imc_driver::DeviceParametersUpdateRequest::MAX_ACCELERATION:
						success = imc_driver__.setMaxAcceleration(device_update.max_acceleration, device_update.device_number-1);
						break;
					case imc_driver::DeviceParametersUpdateRequest::MAX_DECELERATION:
						success = imc_driver__.setMaxDeceleration(device_update.max_deceleration, device_update.device_number-1);
						break;
					case imc_driver::DeviceParametersUpdateRequest::PROFILE_VELOCITY:
						success = imc_driver__.setProfileVelocity(device_update.profile_velocity, device_update.device_number-1);
						break;
					case imc_driver::DeviceParametersUpdateRequest::MAX_PROFILE_VELOCITY:
						success = imc_driver__.setMaxProfileVelocity(device_update.max_profile_velocity, device_update.device_number-1);
						break;
					case imc_driver::DeviceParametersUpdateRequest::POSITIONING_RELATIVE:
						success = imc_driver__.setTargetPositionRelative(device_update.positioning_relative, device_update.device_number-1);
						break;
					case imc_driver::DeviceParametersUpdateRequest::MAX_POSITION_RANGE_LIMIT:
						success = imc_driver__.setMaxPositionRangeLimit(device_update.max_position_range_limit, device_update.device_number-1);
						break;
					case imc_driver::DeviceParametersUpdateRequest::MIN_POSITION_RANGE_LIMIT:
						success = imc_driver__.setMinPositionRangeLimit(device_update.min_position_range_limit, device_update.device_number-1);
						break;
					default:
						success = false;
						break;
				}
				device_response.update_success.push_back(success);
			}
			__device_responses.push_back(device_response);
		}

		// check if some update failed
		for ( auto device_response : __device_responses )
			for ( auto success : device_response.update_success )
				if ( !success )
					return false;
		return true;
	}

	bool getDeviceParameters(imc_driver::SetIMCDriverParameters & __driver_parameters)
	{
		std::vector<int> default_max_allowed_torque;
		if( !getParamAndCheck<int>("default_max_allowed_torque", default_max_allowed_torque) )
			return false;
		std::vector<double> default_profile_acceleration;
		if( !getParamAndCheck<double>("default_profile_acceleration", default_profile_acceleration) )
			return false;
		std::vector<double> default_profile_deceleration;
		if( !getParamAndCheck<double>("default_profile_deceleration", default_profile_deceleration) )
			return false;
		std::vector<double> default_max_acceleration;
		if( !getParamAndCheck<double>("default_max_acceleration", default_max_acceleration) )
			return false;
		std::vector<double> default_max_deceleration;
		if( !getParamAndCheck<double>("default_max_deceleration", default_max_deceleration) )
			return false;
		std::vector<double> default_profile_velocity;
		if ( !getParamAndCheck<double>("default_profile_velocity", default_profile_velocity) )
			return false;
		std::vector<double> default_max_profile_velocity;
		if( !getParamAndCheck<double>("default_max_profile_velocity", default_max_profile_velocity) )
			return false;
		std::vector<bool> default_positioning_relative;
		if( !getParamAndCheck<bool>("default_positioning_relative", default_positioning_relative) )
			return false;
		std::vector<double> default_max_position_range_limit;
		if( !getParamAndCheck<double>("default_max_position_range_limit", default_max_position_range_limit) )
			return false;
		std::vector<double> default_min_position_range_limit;
		if( !getParamAndCheck<double>("default_min_position_range_limit", default_min_position_range_limit) )
			return false;
		for ( unsigned int i = 0; i < num_joints__; i++ )
		{
			imc_driver::DeviceParametersUpdateRequest device_update;
			device_update.device_number = i+1;
			device_update.parameters_to_update.push_back(imc_driver::DeviceParametersUpdateRequest::MAX_ALLOWED_TORQUE);
			device_update.max_allowed_torque = default_max_allowed_torque.at(i);
			device_update.parameters_to_update.push_back(imc_driver::DeviceParametersUpdateRequest::PROFILE_ACCELERATION);
			device_update.profile_acceleration = default_profile_acceleration.at(i);
			device_update.parameters_to_update.push_back(imc_driver::DeviceParametersUpdateRequest::PROFILE_DECELERATION);
			device_update.profile_deceleration = default_profile_deceleration.at(i);
			device_update.parameters_to_update.push_back(imc_driver::DeviceParametersUpdateRequest::MAX_ACCELERATION);
			device_update.max_acceleration = default_max_acceleration.at(i);
			device_update.parameters_to_update.push_back(imc_driver::DeviceParametersUpdateRequest::MAX_DECELERATION);
			device_update.max_deceleration = default_max_deceleration.at(i);
			device_update.parameters_to_update.push_back(imc_driver::DeviceParametersUpdateRequest::PROFILE_VELOCITY);
			device_update.profile_velocity = default_profile_velocity.at(i);
			device_update.parameters_to_update.push_back(imc_driver::DeviceParametersUpdateRequest::MAX_PROFILE_VELOCITY);
			device_update.max_profile_velocity = default_max_profile_velocity.at(i);
			device_update.parameters_to_update.push_back(imc_driver::DeviceParametersUpdateRequest::POSITIONING_RELATIVE);
			device_update.positioning_relative = default_positioning_relative.at(i);
			device_update.parameters_to_update.push_back(imc_driver::DeviceParametersUpdateRequest::MAX_POSITION_RANGE_LIMIT);
			device_update.max_position_range_limit = default_max_position_range_limit.at(i);
			device_update.parameters_to_update.push_back(imc_driver::DeviceParametersUpdateRequest::MIN_POSITION_RANGE_LIMIT);
			device_update.min_position_range_limit = default_min_position_range_limit.at(i);
			__driver_parameters.request.devices.push_back(device_update);
		}
		return true;
	}

	template<class T>
	bool getParamAndCheck(const std::string & __param_name, std::vector<T> & __param_value)
	{
		if( !nh__.getParam(__param_name, __param_value) )
		{
			ROS_ERROR_STREAM("Failed to read parameter " << __param_name << ". This parameter is required to configure the devices.");
			return false;
		}
		if ( __param_value.size() != num_joints__ )
		{
			ROS_ERROR_STREAM("Invalid size of " << __param_name << " parameter. Must match number of joints.");
			return false;
		}
		return true;
	}

};

}

#endif
